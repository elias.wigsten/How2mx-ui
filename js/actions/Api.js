var Dispatcher = require('../dispatcher/ApiDispatcher');
var Api = require('../utils/Api');
var $ = require('jquery');
var ApiClient = require('../utils/ApiClient');

// Constants
var UserConstants = require('../constants/UserConstants');
var GalleryConstants = require('../constants/GalleryConstants');

module.exports = {
    user: {
        galleries: {
            create: function()
            {
                Api.post({
                    uri: 'user/gallery/create',
                    statusCode: {
                        200: function(gallery)
                        {
                            gallery = $.parseJSON(gallery);

                            Dispatcher.serverAction({
                                type: GalleryConstants.CREATED_USER_GALLERY,
                                gallery: gallery
                            });
                        }
                    }
                });
            },

            getAll: function(page_id)
            {
                Api.get({
                    uri: 'user/galleries',
                    data: {
                        page_id: page_id
                    },
                    statusCode: {
                        200: function(response)
                        {
                            response = $.parseJSON(response);
                            response['page_id'] = page_id;

                            Dispatcher.serverAction({
                                type: GalleryConstants.FETCHED_USER_GALLERIES,
                                response: response
                            });
                        }
                    }
                });
            },

            getOne: function(page_id, gallery_id)
            {
                Api.get({
                    uri: 'user/gallery',
                    data: {
                        page_id: page_id,
                        gallery_id: gallery_id
                    },
                    statusCode: {
                        200: function(response)
                        {
                            response = $.parseJSON(response);
                            var gallery = response.gallery;
                            gallery['canRefresh'] = response.canRefresh;

                            Dispatcher.serverAction({
                                type: GalleryConstants.FETCHED_USER_GALLERY,
                                gallery: gallery
                            });
                        }
                    }
                });
            },

            saveTitle: function(gallery)
            {
                Api.post({
                    uri: 'user/gallery/title',
                    data: {
                        gallery_id: gallery.id,
                        title: gallery.title
                    },
                    statusCode: {
                        200: function()
                        {
                            gallery = gallery.set('original_title', gallery.title);

                            Dispatcher.serverAction({
                                type: GalleryConstants.SAVED_USER_GALLERY_CHANGES,
                                gallery: gallery
                            })
                        }
                    }
                });
            },

            items: {
                upload: function(gallery, file)
                {
                    var data = new FormData();
                    data.append('file', file);
                    data.append('gallery_id', gallery.id);

                    Api.upload({
                        uri: 'user/gallery/item',
                        data: data,
                        statusCode: {
                            200: function(item) {
                                item = $.parseJSON(item);

                                // TODO - parse here instead of store
                                item['page_id'] = gallery.page_id;
                                item['gallery_id'] = gallery.id;

                                Dispatcher.serverAction({
                                    type: GalleryConstants.UPLOADED_FILE,
                                    item: item
                                });
                            }
                        }
                    });
                },

                get: function(gallery)
                {
                    Api.get({
                        uri: 'user/gallery/items',
                        data: {
                            page_id: gallery.page_id,
                            gallery_id: gallery.id,
                            to: gallery.firstItemId()
                        },
                        statusCode: {
                            200: function(response)
                            {
                                response = $.parseJSON(response);

                                gallery = gallery.addItems(response.items);
                                gallery = gallery.setRefresh(response.canRefresh);

                                Dispatcher.serverAction({
                                    type: GalleryConstants.FETCHED_USER_GALLERY_ITEMS,
                                    gallery: gallery
                                });
                            }
                        }
                    });
                },

                next: function(gallery)
                {
                    Api.get({
                        uri: 'user/gallery/items/next',
                        data: {
                            page_id: gallery.page_id,
                            gallery_id: gallery.id,
                            from: gallery.lastItemId()
                        },
                        statusCode: {
                            200: function(response)
                            {
                                response = $.parseJSON(response);

                                gallery = gallery.addItems(response.items);
                                gallery = gallery.setRefresh(response.canRefresh);

                                Dispatcher.serverAction({
                                    type: GalleryConstants.FETCHED_USER_GALLERY_ITEMS,
                                    gallery: gallery
                                });
                            }
                        }
                    });
                },

                saveDescription: function(item)
                {
                    console.log('save item description', item);
                    Api.post({
                        uri: 'user/gallery/item/description',
                        data: {
                            id: item.id,
                            description: item.description
                        },
                        statusCode: {
                            200: function()
                            {
                                Dispatcher.serverAction({
                                    type: GalleryConstants.UPDATED_ITEM_DESCRIPTION,
                                    item: item
                                });
                            },
                            401: function(r)
                            {
                                console.log('could not update');
                            }
                        }
                    });
                },

                deleteMany: function(items)
                {
                    if (items.length > 0) {
                        var gallery_id = items[0].gallery_id;
                        var itemList = [];

                        items.forEach(function(item, i) {
                            itemList.push(item.id);
                        });

                        Api.post({
                            uri: 'user/gallery/items/delete',
                            data: {
                                gallery_id: gallery_id,
                                items: itemList
                            }
                        });
                    }
                },

                deleteOne: function(item)
                {
                    // TODO
                    console.log('TODO');
                }
            }
        },
        feed: {
            get: function(page_id, posts) {
                ApiClient.feed.get(
                    page_id,
                    posts,
                    UserConstants.FETCHED_USER_POSTS
                );
            },

            getNext: function(request) {
                ApiClient.feed.getNext(
                    request,
                    UserConstants.FETCHED_NEXT_FEED_EVENTS
                );
            },

            create: function(recipient, message)
            {
                ApiClient.feed.create({
                    page_id: recipient.page_id,
                    message: message
                }, UserConstants.CREATED_POST);
            },

            deleteOne: function(post)
            {
                ApiClient.feed.deleteOne({
                    post: post
                }, UserConstants.DELETED_POST);
            }
        }
    }
};