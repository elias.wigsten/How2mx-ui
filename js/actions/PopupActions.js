var AppDispatcher = require('../dispatcher/AppDispatcher');

// Constants
var PopupConstants = require('../constants/PopupConstants');

module.exports = {
    closePopup: function()
    {
        AppDispatcher.viewAction({
            type: PopupConstants.CLOSE_POPUP
        });
    }
};