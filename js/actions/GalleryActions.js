var AppDispatcher = require('../dispatcher/AppDispatcher');
var Api = require('../utils/Api');
var $ = require('jquery');

// Constants
var GalleryConstants = require('../constants/GalleryConstants');

module.exports = {
    remember: function(page_id, gallery)
    {
        AppDispatcher.appAction({
            type: GalleryConstants.REMEMBER,
            page_id: page_id,
            gallery: gallery
        });
    },

    forget: function(page_id, gallery)
    {
        AppDispatcher.viewAction({
            type: GalleryConstants.FORGET,
            page_id: page_id,
            gallery: gallery
        });
    },

    createUserGallery: function()
    {
        Api.post({
            uri: 'user/gallery/create',
            statusCode: {
                200: function(gallery)
                {
                    gallery = $.parseJSON(gallery);

                    AppDispatcher.serverAction({
                        type: GalleryConstants.CREATED_USER_GALLERY,
                        gallery: gallery
                    });
                },
                500: function(r)
                {
                    console.log(r.responseText);
                }
            }
        });
    },

    uploadFile: function(gallery, file)
    {
        var data = new FormData();
        data.append('file', file);
        data.append('gallery_id', gallery.id);

        Api.upload({
            uri: 'user/gallery/item',
            data: data,
            statusCode: {
                200: function(item) {
                    item = $.parseJSON(item);
                    item['page_id'] = gallery.page_id;
                    item['gallery_id'] = gallery.id;

                    AppDispatcher.serverAction({
                        type: GalleryConstants.UPLOADED_FILE,
                        item: item
                    });
                },
                500: function(r) {
                    console.log(r.responseText);
                }
            }
        });
    },

    saveItemDescription: function(item)
    {
        console.log('save item description');
        console.log(item);
    },

    deleteItems: function(items)
    {
        AppDispatcher.viewAction({
            type: GalleryConstants.DELETE_ITEMS,
            items: items
        });
    },

    deleteGalleryItemsOnServer: function(items)
    {
        if (items.length > 0) {
            var gallery_id = items[0].gallery_id;

            items.forEach(function(item, i) {
                items[i] = item.id;
            });

            console.log('remove items from gallery: ', gallery_id, items);
        }
    }
};