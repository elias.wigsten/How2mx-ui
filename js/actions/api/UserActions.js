// Dispatcher
var AppDispatcher = require('../../dispatcher/AppDispatcher');
var $ = require('jquery');

// Constants
var UserConstants = require('../../constants/UserConstants');

// Stores
var Api = require('../../utils/Api');
var SelfStore = global.SelfStore;

module.exports = {
    fetchUser: function(page_id)
    {
        Api.get({
            uri: 'user',
            data: {
                page_id: page_id
            },
            statusCode: {
                200: function(user)
                {
                    user = $.parseJSON(user);

                    AppDispatcher.serverAction({
                        type: UserConstants.FETCHED_USER_PROFILE,
                        user: user
                    });
                }
            }
        });
    },

    addFriend: function(user)
    {
        Api.post({
            uri: 'sendFriendRequest',
            data: {
                user_id: user.get('id')
            },
            statusCode: {
                200: function(r)
                {
                    AppDispatcher.serverAction({
                        type: UserConstants.FRIEND_REQUEST_SENT,
                        friend: user
                    });
                }
            }
        });
    },

    removeFriend: function(user)
    {
        Api.post({
            uri: 'removeFriend',
            data: {
                user_id: user.get('id')
            },
            statusCode: {
                200: function()
                {
                    AppDispatcher.serverAction({
                        type: UserConstants.FRIEND_REMOVED,
                        friend: user
                    });
                }
            }
        });
    },

    acceptFriend: function(user)
    {
        Api.post({
            uri: 'acceptFriend',
            data: {
                user_id: user.get('id')
            },
            statusCode: {
                200: function () {
                    AppDispatcher.serverAction({
                        type: UserConstants.FRIEND_REQUEST_ACCEPTED,
                        friend: user
                    });
                }
            }
        });
    },

    fetchFriendsFor: function(user)
    {
        Api.get({
            uri: 'user/friends',
            data: {
                user_id: user.get('id')
            },
            statusCode: {
                200: function(response)
                {
                    response = $.parseJSON(response);
                    var type = response.sourceOwner ? UserConstants.FETCHED_FRIENDS : UserConstants.FETCHED_USER_FRIENDS;

                    AppDispatcher.serverAction({
                        type: type,
                        data: {
                            page_id: user.get('page_id'),
                            friends: response.friends
                        }
                    });
                }
            }
        });
    },

    uploadFiles: function(files)
    {
        var data = new FormData();

        $.each(files, function(key, file) {
            data.append(key, file);
        });

        $.ajax({
            url: 'http://localhost:8888/how2mx/project/API/public/media/upload',
            type: 'POST',
            data: data,
            cache: false,
            dataTypes: 'json',
            processData: false,
            contentType: false,
            statusCode: {
                200: function(r)
                {
                    console.log(r);
                }
            }
        });
    },

    createGallery: function(files)
    {
        AppDispatcher.viewAction({
            type: UserConstants.NEW_GALLERY,
            files: files
        });
    },

    saveGallery: function(page_id, gallery)
    {
        if (gallery.get('id')) {
            this.updateGallery(page_id, gallery);
        } else {
            this.saveNewGallery(page_id, gallery);
        }
    },

    saveNewGallery: function(page_id, gallery)
    {
        var data = {
            title: gallery.get('title'),
            items: gallery.get('unsaved_items')
        };

        console.log(gallery, data);

        /*Api.post({
             uri: 'user/gallery/create',
             data: data,
             statusCode: {
                 200: function(r)
                 {
                    console.log(r);
                 }
             }
         });*/
    },

    updateGallery: function(page_id, gallery)
    {
        // TODO - update existing gallery
    }
};