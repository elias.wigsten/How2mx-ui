// Dispatcher
var AppDispatcher = require('../../dispatcher/AppDispatcher');
var $ = require('jquery');

// Constants
var AuthConstants = require('../../constants/AuthConstants');

// Stores
var Api = require('../../utils/Api');

module.exports = {
    register: function(formValues)
    {
        var requestData = {
            f_name: formValues.f_name,
            l_name: formValues.l_name,
            email: formValues.email,
            gender: formValues.gender,
            birth_date: formValues.birth_year + '-' + formValues.birth_month + '-' + formValues.birth_day,
            password: formValues.password,
            password_confirmation: formValues.password_confirmation
        };

        Api.post({
            uri: 'register',
            data: requestData,
            statusCode: {
                200: function()
                {
                    AppDispatcher.serverAction({
                        type: AuthConstants.REGISTER_SUCCESS
                    });
                },
                422: function(responseData)
                {
                    var formErrors = $.parseJSON(responseData.responseText);

                    AppDispatcher.serverAction({
                        type: AuthConstants.REGISTER_FAILED,
                        formErrors: formErrors,
                        formValues: formValues
                    });
                }
            }
        });
    },

    login: function(credentials)
    {
        Api.post({
            uri: 'login',
            data: credentials,
            statusCode: {
                200: function(response)
                {
                    var user = $.parseJSON(response);

                    AppDispatcher.serverAction({
                        type: AuthConstants.LOGIN_SUCCESS,
                        user: user
                    });
                },
                401: function()
                {
                    AppDispatcher.serverAction({
                        type: AuthConstants.LOGIN_FAILED,
                        error: 'Incorrect email or password'
                    });
                }
            }
        });
    },

    remembered: function()
    {
        Api.get({
            uri: 'remembered',
            statusCode: {
                200: function(response)
                {
                    var user = $.parseJSON(response);

                    AppDispatcher.serverAction({
                        type: AuthConstants.LOGIN_SUCCESS,
                        user: user
                    });
                },
                203: function()
                {
                    AppDispatcher.serverAction({
                        type: AuthConstants.UNAUTHORIZED
                    });
                }
            }
        });
    },

    logout: function()
    {
        Api.post({
            uri: 'logout',
            statusCode: {
                200: function()
                {
                    AppDispatcher.serverAction({
                        type: AuthConstants.LOGOUT
                    });
                }
            }
        });
    },

    resetLoginState: function()
    {
        AppDispatcher.appAction({
            type: AuthConstants.RESET_LOGIN_STATE
        });
    }
};