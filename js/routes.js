var React           = require('react');
var Router          = require('react-router');
var IndexRoute      = Router.IndexRoute;
var Route           = Router.Route;

var App             = require('./components/App');
var Home            = require('./components/routes/home/Page');
var Register        = require('./components/routes/register/Page');
var UserPage        = require('./components/routes/user/Page');
var UserStream      = require('./components/routes/user/Stream');
var UserGalleries   = require('./components/routes/user/Galleries');
var UserGallery     = require('./components/routes/user/Gallery');
var NewGallery      = require('./components/routes/user/NewGallery');
var UserFriends     = require('./components/routes/user/Friends');

module.exports = (
    <Route path="/" component={App} >
        <IndexRoute component={Home} />
        <Route path="register" component={Register} />
        <Route path="user/:page_id" component={UserPage} >
            <IndexRoute component={UserStream} />
            <Route path="galleries" component={UserGalleries} />
            <Route path="gallery" >
                <Route path="new" component={NewGallery} />
                <Route path=":gallery_token" component={UserGallery} />
            </Route>
            <Route path="friends" component={UserFriends} />
        </Route>
    </Route>
);