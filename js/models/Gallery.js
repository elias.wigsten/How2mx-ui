var Immutable = require('immutable');

// Utils
var Timestamps = require('../utils/Timestamps');
var DateUtils = require('../utils/DateUtils');

// Stores
var SelfStore = global.SelfStore;

// Models
var NewGalleryItemModel = require('./NewGalleryItem');
var ItemListModel = require('./GalleryItemList');

var GalleryModel = Immutable.Record({
    fetched_at: Timestamps.now(),
    canRefresh: true,
    id: undefined,
    title: undefined,
    original_title: undefined,
    page_id: undefined,
    preview: undefined,
    items: ItemListModel.create(),
    changedItems: new Immutable.OrderedMap(),
    files_to_upload: undefined,
    created_at: undefined,
    updated_at: undefined,
    lastItemId: function()
    {
        var last = this.items.all.last();

        return last ? last.id : undefined;
    },
    firstItemId: function()
    {
        var first = this.items.all.first();

        return first ? first.id : undefined;
    },
    setRefresh: function(canRefresh)
    {
        var gallery = this.set('fetched_at', Timestamps.now());

        if (canRefresh !== undefined) {
            return gallery.set('canRefresh', canRefresh);
        }

        return gallery;
    },
    shouldFetchNewItems: function()
    {
        return this.fetched_at && Timestamps.now() >
        (this.fetched_at + Timestamps.duration(30, Timestamps.SECONDS));
    },
    addItems: function(items)
    {
        if (items.length > 0) {
            if (this.items.all.size === 0) {
                return this.createItems(items);
            } else {
                if (items[0].id < this.items.all.last().id) {
                    return this.setItems(
                        this.items.append(this.page_id, this.id, items)
                    );
                } else {
                    return this.setItems(
                        this.items.unshift(this.page_id, this.id, items)
                    );
                }
            }
        }

        return this;
    },
    updateItemDescription: function(item)
    {
        return this.setItems(
            this.items.updateDescription(item)
        );
    },
    select: function (item)
    {
        return this.setItems(
            this.items.select(item)
        );
    },
    deselect: function (item)
    {
        return this.setItems(
            this.items.deselect(item)
        );
    },
    titleIsValid: function()
    {
        return this.title.length > 0 && this.title.length < 32;
    },
    titleChanged: function()
    {
        return this.title !== this.original_title
    },
    changeItem: function(item)
    {
        return this.set('items',
            this.items.changeItem(item)
        );
    },
    setItems: function(items)
    {
        return this.set('items', items);
    },
    createItems: function(items)
    {
        return this.setItems(
            this.items.createItems(this.page_id, this.id, items)
        );
    },
    removeItems: function(itemsToRemove)
    {
        var items = this.items;

        itemsToRemove.forEach(function(item) {
            items = items.remove(item);
        });

        return this.set('items', items);
    }
});

module.exports = {
    create: function(gallery)
    {
        if (gallery) {
            var newGallery = new GalleryModel(gallery);
            return newGallery.set('original_title', gallery.title);
        }
    },

    addOneItem: function(item, model)
    {
        var updatedItems = Immutable.OrderedMap([
            [item.id, item]
        ]).merge(model.items.all);

        updatedItems = model.items.set('all', updatedItems);

        return model.set('items', updatedItems);
    },

    createNewGallery: function(files, owner_id)
    {
        var galleryItems = new Immutable.List();

        if (files) {
            files.forEach( function(file, i) {
                galleryItems = galleryItems.set(i, new NewGalleryItemModel({
                    file: file
                }));
            });
        }

        return new GalleryModel({
            owner_id: owner_id,
            unsaved_items: galleryItems,
            saved: false
        });
    },

    addUnsavedItems: function(files, gallery)
    {
        var items = gallery.get('unsaved_items');

        if (files) {
            files.forEach( function(file) {
                var unique = gallery.unsaved_items.every( function(existingFile) {
                    return file.name !== existingFile.file.name;
                });

                if (unique) {
                    items = items.unshift(new NewGalleryItemModel({
                        file: file
                    }));
                }
            });
        }

        return gallery.set('unsaved_items', items);
    }
};