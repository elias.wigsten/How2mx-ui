var Immutable = require('immutable');

// Utils
var Timestamps = require('../utils/Timestamps');

var FriendshipModel = Immutable.Record({
    fetched_at: undefined,
    status: undefined,
    source: undefined,
    recipient: undefined
});

module.exports = {
    create: function(friend)
    {
        if (friend) {
            return new FriendshipModel({
                fetched_at: Timestamps.now(),
                status: friend.status,
                source: friend.user1,
                recipient: friend.user2
            });
        }

        if (friend === null) {
            return null;
        }
    },

    setPending: function(user, friend)
    {
        if (user && user.info && user.info.friendship === null && friend) {
            var friendship = this.create({
                status: 0,
                user1: friend.get('id'),
                user2: user.info.get('id')
            });

            var updatedUserInfo = user.info.set('friendship', friendship);

            return user.set('info', updatedUserInfo);
        }
    },

    setAccepted: function(user)
    {
        if (user && user.info) {
            var friendship = user.info.get('friendship');

            if (friendship && friendship.get('status') === 0) {
                var updatedFriendship = friendship.set('status', 1);
                var updatedUserInfo = user.info.set('friendship', updatedFriendship);

                return user.set('info', updatedUserInfo);
            }
        }
    },

    clearFriendshipFor: function(user)
    {
        if (user && user.info && user.info.friendship !== null) {
            var updatedFriendship = user.info.set('friendship', null);
            return user.set('info', updatedFriendship);
        }
    }
};