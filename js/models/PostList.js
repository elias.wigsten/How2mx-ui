"use strict";
var Immutable = require('immutable');
var Post = require('./Post');
var PagePreview = require('./PagePreview');

// Utils
var Timestamps = require('../utils/Timestamps');

module.exports = Immutable.Record({
    fetched_at: Timestamps.now(),
    page_id: undefined,
    canRefresh: null,
    all: Immutable.OrderedMap(),
    syncWith: function(posts)
    {
        if (this.page_id === posts.page_id) {
            let postList = this.set('fetched_at', posts.fetched_at);
            postList = postList.setRefresh(posts.canRefresh);

            if (posts.hasPosts()) {
                postList = postList.addMany(posts.all);
            }

            return postList;
        }

        return this;
    },
    createMany: function(posts)
    {
        var newPosts = Immutable.OrderedMap();

        posts.forEach(function(post) {
            newPosts = newPosts.set(post.id, this.createOne(post));
        }.bind(this));

        return this.addMany(newPosts);
    },
    createOne: function(post)
    {
        post.author = new PagePreview(post.author);
        post.recipient = new PagePreview(post.recipient);

        post = new Post(post);
        return post.setDeletable(global.SelfStore.getPageId());
    },
    addOne: function(post)
    {
        var tempList = new Immutable.OrderedMap();

        return this.setAll(
            this.unshift(
                tempList.set(post.id, this.createOne(post))
            )
        );
    },
    addMany: function(posts)
    {
        if (this.all.size > 0) {
            if (posts[0].id > this.all.last().id) {
                posts = this.append(posts);
            } else {
                posts = this.unshift(posts);
            }
        }

        return this.setAll(posts);
    },
    append: function(posts) {
        return this.all.merge(posts);
    },
    unshift: function(posts) {
        return posts.merge(this.all);
    },
    setRefresh: function(canRefresh) {
        if (canRefresh === null) {
            return this;
        }

        return this.set('canRefresh', canRefresh);
    },
    setAll: function(all)
    {
        return this.set('all', all);
    },
    deleteOne: function(post_id)
    {
        return this.setAll(
            this.all.remove(post_id)
        );
    },
    getFirstId: function()
    {
        if (this.hasPosts()) {
            return this.all.first().id;
        }

        return null;
    },
    getLastId: function()
    {
        if (this.hasPosts()) {
            return this.all.last().id;
        }
    },
    hasPosts: function()
    {
        return this.all.size > 0;
    }
});
