var Immutable = require('immutable');

module.exports = Immutable.Record({
    page_id: null,
    gallery_id: null,
    filename: null
});