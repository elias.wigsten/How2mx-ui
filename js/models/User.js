var Immutable = require('immutable');

// Models
var FriendshipModel = require('./Friendship');

// Utils
var Timestamps = require('../utils/Timestamps');

var UserModel = Immutable.Record({
    id: undefined,
    auth_level: 'GUEST',
    page_id: undefined,
    page_name: undefined,
    f_name: undefined,
    l_name: undefined,
    gender: undefined,
    age: undefined,
    profile_img: undefined,
    created_at: undefined,
    fetched_at: undefined,
    friendship: undefined
});

module.exports = {
    createUser: function(user)
    {
        return new UserModel({
                id: user.id,
                auth_level: 'USER',
                page_id: user.page_id,
                f_name: user.f_name,
                l_name: user.l_name,
                gender: user.gender,
                profile_img: user.profile_img,
                age: user.birth_date,
                created_at: user.created_at,
                fetched_at: Timestamps.now(),
                friendship: FriendshipModel.create(user.friendship)
            });
    },

    createGuest: function()
    {
        return new UserModel();
    }
};