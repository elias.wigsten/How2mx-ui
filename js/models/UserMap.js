var Immutable = require('immutable');

module.exports = Immutable.Record({
    info: undefined,
    posts: undefined,
    galleries: undefined,
    friends: undefined
});