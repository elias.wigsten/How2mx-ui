var Immutable = require('immutable');

var UserInfoModel = require('./UserInfo');

module.exports = Immutable.Record({
    id: undefined,
    author: undefined,
    recipient: undefined,
    deletable: false,
    message: undefined,
    created_at: undefined,
    updated_at: undefined,
    setDeletable: function(self_page_id)
    {
        var deletable = this.author.id === self_page_id || this.recipient.id === self_page_id;

        return this.set('deletable', deletable);
    }
});