var Immutable = require('immutable');

var GalleryItem = Immutable.Record({
    id: undefined,
    page_id: undefined,
    gallery_id: undefined,
    src: undefined,
    description: undefined,
    original_description: undefined,
    media_type: undefined,
    created_at: undefined,
    updated_at: undefined,
    selected: false,
    updateDescription: function(description)
    {
        var item = this.set('description', description);
        return item.set('original_description', description);
    },
    descriptionChanged: function()
    {
        return this.original_description != this.description;
    },
    changed: function()
    {
        return this.descriptionChanged();
    }
});

module.exports = {
    create: function(item) {
        var newItem = new GalleryItem(item);
        return newItem.set('original_description', newItem.description);
    }
};