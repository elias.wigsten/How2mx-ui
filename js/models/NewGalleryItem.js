var Immutable = require('immutable');

module.exports = Immutable.Record({
    description: '',
    selected: false,
    file: undefined
});