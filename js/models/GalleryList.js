var Immutable = require('immutable');
var GalleryModel = require('./Gallery');

// Utils
var Timestamps = require('../utils/Timestamps');

var GalleryItems = Immutable.Record({
    fetched_at: undefined,
    saved: undefined,
    unsaved: undefined
});

var GalleryListModel = Immutable.Record({
    page_id: undefined,
    fetched_at: Timestamps.now(),
    canRefresh: false,
    all: new Immutable.OrderedMap(),
    add: function(gallery)
    {
        var items = gallery.items;
        delete gallery.items;

        gallery = GalleryModel.create(gallery);
        gallery = gallery.addItems(items);

        return this.setAll(
            this.all.set(gallery.id, gallery)
        );
    },
    append: function(galleries)
    {
        return this.setAll(
            this.all.merge(galleries)
        );
    },
    unshift: function(galleries)
    {
        return this.setAll(
            galleries.merge(this.all)
        );
    },
    createList: function(galleries)
    {
        var newGalleries = new Immutable.OrderedMap();

        galleries.forEach(function(gallery) {
            gallery = GalleryModel.create(gallery);
            newGalleries = newGalleries.set(gallery.id, gallery);
        });

        return newGalleries;
    },
    addItems: function(gallery_id, items, canRefresh)
    {
        console.log('add items');
        var gallery = this.all.get(gallery_id);

        if (gallery) {
            return this.setAll(
                this.all.set(gallery_id,
                    gallery.addMany(items, canRefresh)
                )
            );
        } else {
            return this;
        }
    },
    replace: function(gallery)
    {
        return this.setAll(
            this.all.set(gallery.id, gallery)
        );
    },
    setAll: function(all)
    {
        return this.set('all', all);
    },
    addFilesToUpload: function(gallery_id, files)
    {
        var gallery = this.all.get(gallery_id).set('files_to_upload', files);

        return this.replace(gallery);
    },
    shouldUpdate: function()
    {
        return this.fetched_at > (Timestamps.now() + Timestamps.duration(60, Timestamps.SECONDS));
    }
});

module.exports = {
    init: function(page_id)
    {
        return new GalleryListModel({
            page_id: page_id
        });
    },

    createList: function(galleriesResponse)
    {
        var galleries = new GalleryListModel({
            fetched_at: 'timestamp'
        });

        for (var i in galleriesResponse) {
            if (galleriesResponse.hasOwnProperty(i)) {
                var gallery = this.create(galleriesResponse[i]);

                galleries.all.set(gallery.get('id'), gallery);
            }
        }

        return galleries;
    },

    add: function(gallery, model)
    {
        var updatedGalleries = Immutable.OrderedMap([
            [gallery.id, gallery]
        ]).merge(model.all);

        return model.set('all', updatedGalleries);
    },

    replaceOne: function(gallery, model)
    {
        var updatedGalleries = model.all.set(gallery.id, gallery);
        return model.set('all', updatedGalleries);
    },

    create: function(gallery)
    {
        return new GalleryModel({
            fetched_at: 'timestamp',
            id: gallery.id,
            title: gallery.title,
            owner: gallery.page_id,
            preview: gallery.preview
        });
    }
};