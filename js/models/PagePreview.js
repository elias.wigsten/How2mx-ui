var Immutable = require('immutable');

// Models
var Img = require('./Image');

module.exports = Immutable.Record({
    id: undefined,
    display_name: undefined,
    description: undefined,
    profile_img: null,
    getProfileImage: function()
    {
        return new Img({
            page_id: this.page_id,
            filename: this.profile_img
        });
    }
});