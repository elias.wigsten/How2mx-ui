var Immutable = require('immutable');
var UserModel = require('./User');

// Utils
var Timestamps = require('../utils/Timestamps');

var FriendListModel = Immutable.Record({
    fetched_at: undefined,
    all: new Immutable.OrderedMap()
});

module.exports = {
    create: function(friendsResponse)
    {
        var friends = new Immutable.OrderedMap();

        for (var i in friendsResponse) {
            if (friendsResponse.hasOwnProperty(i)) {
                var friend = UserModel.createUser(friendsResponse[i]);

                friends = friends.set(friend.get('page_id'), friend);
            }
        }

        return new FriendListModel({
            fetched_at: Timestamps.now(),
            all: friends
        });
    },

    add: function(friend, user)
    {
        if (user && user.friends && friend) {
            var updatedFriendList = new Immutable.OrderedMap([
                [friend.get('page_id'), friend]
            ]).merge(user.friends.get('all'));

            var updatedFriends = user.friends.set('all', updatedFriendList);
            return user.set('friends', updatedFriends);
        }
    },

    remove: function(friend, user) {
        if (user && user.friends) {
            var updatedFriendList = user.friends.all.remove(friend.get('page_id'));
            var updatedFriends = user.friends.set('all', updatedFriendList);

            return user.set('friends', updatedFriends);
        }

        return user;
    },

    makeEmpty: function()
    {
        return new FriendListModel();
    }
};