var Immutable = require('immutable');
var Timestamps = require('../utils/Timestamps');

// Models
var Item = require('./GalleryItem');

var GalleryItemListModel = Immutable.Record({
    fetched_at: undefined,
    all: new Immutable.OrderedMap(),
    updateDescription: function(item)
    {
        var orgItem = this.all.get(item.id);
        item = orgItem.updateDescription(item.description);

        return this.setAll(
            this.all.set(item.id, item)
        );
    },
    select: function(item)
    {
        return this.setItemSelected(item, true);
    },
    deselect: function(item)
    {
        return this.setItemSelected(item, false);
    },
    setItemSelected: function(item, selected)
    {
        item = this.all.get(item.id);
        item = item.set('selected', selected);

        return this.setAll(
            this.all.set(item.id, item)
        );
    },
    changeItem: function(item) {
        return this.setAll(
            this.all.set(item.id, item)
        );
    },
    createItems: function(page_id, gallery_id, items)
    {
        var all = this.all;

        items.forEach(function(item) {
            item.page_id = page_id;
            item.gallery_id = gallery_id;

            all = all.set(item.id, Item.create(item));
        });

        return this.setAll(all);
    },
    append: function(page_id, gallery_id, items)
    {
        var itemList = this.makeNewItemList(page_id, gallery_id, items);

        return this.setAll(
            this.all.merge(itemList)
        );
    },
    unshift: function(page_id, gallery_id, items)
    {
        var itemList = this.makeNewItemList(page_id, gallery_id, items);

        return this.setAll(
            itemList.merge(this.all)
        );
    },
    makeNewItemList: function(page_id, gallery_id, items)
    {
        var all = new Immutable.OrderedMap();

        items.forEach(function(item) {
            item.page_id = page_id;
            item.gallery_id = gallery_id;

            all = all.set(item.id, Item.create(item));
        });

        return all;
    },
    setAll: function(all)
    {
        return this.set('all', all);
    },
    remove: function(item) {
        return this.set('all', this.all.remove(item.id));
    }
});

module.exports = {
    create: function()
    {
        return new GalleryItemListModel();
    }
};