var Immutable = require('immutable');

var FriendshipModel = require('./Friendship');

var UserInfoModel = Immutable.Record({
    f_name: undefined,
    l_name: undefined,
    profile_img: undefined,
    page_id: undefined,
    friendship: undefined
});

module.exports = {
    getAuthorFrom: function(post)
    {
        return new UserInfoModel({
            f_name: post.author_f_name,
            l_name: post.author_l_name,
            profile_img: post.author_profile_img,
            page_id: post.author_id
        });
    },

    getRecipientFrom: function(post)
    {
        return new UserInfoModel({
            f_name: post.recipient_f_name,
            l_name: post.recipient_l_name,
            profile_img: post.recipient_profile_img,
            page_id: post.recipient_id
        });
    },

    getFriendFrom: function(friend)
    {
        var friendship = FriendshipModel.create(friend);

        return new UserInfoModel({
            f_name: friend.f_name,
            l_name: friend.l_name,
            profile_img: friend.profile_img,
            page_id: friend.page_id,
            friendship: friendship
        });
    }
};