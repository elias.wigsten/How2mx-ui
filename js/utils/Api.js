"use strict";
var $ = require('jquery');

var Request = require('./Request');
var Queue = require('./ApiQueue');

module.exports = {
    queue: Queue,
    data: null,
    callbacks: null,
    isBusy: false,

    get: function(requestData)
    {
        var request = this.buildRequest(requestData, 'GET');

        this.handle(request);
    },

    post: function(requestData)
    {
        var request = this.buildRequest(requestData, 'POST');

        this.handle(request);
    },

    upload: function(requestData)
    {
        var request = this.buildRequest(requestData, 'POST');

        request['processData'] = false;
        request['contentType'] = false;

        this.handle(request);
    },

    buildRequest: function(request, method)
    {
        return new Request().setMethod(method)
                            .setRoute(request.uri)
                            .setStatusCodes(request.statusCode)
                            .setData(request.data)
                            .get();
    },

    handle: function(request)
    {
        //this.queue.add(request);

        this.send(request);
    },

    send: function(request)
    {
        this.isBusy = true;

        $.ajax(request).always(function() {
            this.queue.done();

            if (this.queue.isEmpty()) {
                this.isBusy = false;
            }
            else {
                this.send(this.queue.next());
            }
        }.bind(this));
    },

    /** For upload progress bar ( not used )
     *
     * @param callbackComponent
     * @param index
     * @returns {Function}
     */
    uploadListener: function(callbackComponent, index)
    {
        return function() {
            var xhr = new window.XMLHttpRequest();

            //Upload progress
            xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with upload progress
                    callbackComponent.onProgress(percentComplete);
                }
            }, false);

            xhr.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;

                    if (percentComplete === 1) {
                        callbackComponent.onUploadComplete(index);
                    }
                }
            }, false);

            return xhr;
        };
    },

    // For download progress bar ( not used )
    downloadListener: function(callbackComponent)
    {
        return function()
        {
            var xhr = new window.XMLHttpRequest();

            //Download progress
            xhr.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with download progress
                    console.log('downloading');
                    console.log(percentComplete);
                }
            }, false);

            return xhr;
        };
    }
};