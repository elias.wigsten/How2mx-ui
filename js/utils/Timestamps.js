module.exports = {
    SECONDS:    1000,
    MINUTES:    1000 * 60,
    HOURS:      1000 * 60 * 60,
    DAYS:       1000 * 60 * 60 * 24,
    WEEKS:      1000 * 60 * 60 * 24 * 7,
    MONTHS:     1000 * 60 * 60 * 24 * 30,
    YEARS:      1000 * 60 * 60 * 24 * 365,

    now: function()
    {
        return this.convertDate(new Date());
    },

    duration: function(amount, interval)
    {
        interval = interval ? interval : this.SECONDS;

        return amount * interval;
    },

    timePassedSince: function(date)
    {
        var diff = this.now() - this.convertStringDate(date);

        if (diff < this.MINUTES) {
            return this.getTimePassedText(diff, this.SECONDS);
        } else if (diff < this.HOURS) {
            return this.getTimePassedText(diff, this.MINUTES);
        } else if (diff < this.DAYS) {
            return this.getTimePassedText(diff, this.HOURS);
        } else if (diff < this.WEEKS) {
            return this.getTimePassedText(diff, this.DAYS);
        } else if (diff < this.MONTHS) {
            return this.getTimePassedText(diff, this.WEEKS);
        } else if (diff < this.YEARS) {
            return this.getTimePassedText(diff, this.MONTHS);
        }

        return this.getTimePassedText(diff, this.YEARS);
    },

    getTimePassedText: function(diff, interval)
    {
        diff = Math.floor(diff / interval);

        return {
            diff: diff,
            interval: interval
        };
    },

    intervalToText: function(interval)
    {
        switch (interval) {
            case this.SECONDS:
                return "seconds";
            case this.MINUTES:
                return "minutes";
            case this.HOURS:
                return "hours";
            case this.DAYS:
                return "days";
            case this.WEEKS:
                return "weeks";
            case this.MONTHS:
                return "months";
            default:
                return "years";
        }
    },

    convertDate: function(date)
    {
        return Math.floor(date.getTime());
    },

    convertStringDate: function(stringDate)
    {
        var date = new Date(stringDate);

        return this.convertDate(date);
    }
};
