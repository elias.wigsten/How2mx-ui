module.exports = {
    STORAGE_PATH: 'http://localhost:8888/how2mx/project/storage/pages/',
    DEFAULT_PROFILE_IMAGE_PATH: 'defaultProfileImage.png',

    size: {
        ORIGINAL: 'original',
        PREVIEW: 'preview',
        THUMBNAIL: 'thumbnail'
    },

    DEFAULT_PROFILE_IMAGE: function()
    {
        return this.makePath([
            this.DEFAULT_PROFILE_IMAGE_PATH
        ]);
    },

    makeImage: function(image, size)
    {
        if (image.filename === null) {
            return this.DEFAULT_PROFILE_IMAGE();
        }

        var parameters = this.extractParametersFrom(image, size);

        return this.makePath(parameters);
    },

    extractParametersFrom: function(image, size)
    {
        size = !size ? this.size.THUMBNAIL : size;
        var parameters = [];

        image.forEach(function(param, key) {
            if (key == "filename") {
                parameters.push(size);
            }

            parameters.push(param);
        });
    },

    getPreview: function(page_id, gallery_id, filename)
    {
        return this.makePath([
            page_id,
            gallery_id,
            this.size.PREVIEW,
            filename
        ]);
    },

    getPreviewFor: function(item)
    {
        return this.makePath([
            item.page_id,
            item.gallery_id,
            this.size.PREVIEW,
            item.src
        ]);
    },

    makePath: function(parameters)
    {
        return this.STORAGE_PATH + parameters.join("/");
    }
};