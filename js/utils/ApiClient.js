"use strict";
var AppDispatcher = require('../dispatcher/AppDispatcher');
var Api = require('./Api');
var $ = require('jquery');

// Models
var PostList = require('../models/PostList');

module.exports = {
    feed: {
        get: function(page_id, posts, action) {
            var first_id = !posts ? null : posts.getFirstId();

            Api.get({
                uri: "feed",
                data: {
                    page_id: page_id,
                    to: first_id
                },
                statusCode: {
                    200: function(response) {
                        response = $.parseJSON(response);
                        response['page_id'] = page_id;

                        let newPosts = new PostList(response);
                        newPosts = newPosts.createMany(response.posts);

                        AppDispatcher.serverAction({
                            type: action,
                            posts: newPosts
                        });
                    }
                }
            });
        },

        getNext: function(request, action) {
            Api.get({
                uri: "feed",
                data: request,
                statusCode: {
                    200: function(response) {
                        response = $.parseJSON(response);

                        AppDispatcher.serverAction({
                            type: action,
                            data: {
                                page_id: request.page_id,
                                posts: response.posts
                            }
                        });
                    }
                }
            });
        },

        create: function(request, action) {
            Api.post({
                uri: 'user/post',
                data: request,
                statusCode:
                {
                    200: function(response)
                    {
                        response = $.parseJSON(response);

                        AppDispatcher.serverAction({
                            type: action,
                            post: response.post
                        });
                    }
                }
            });
        },

        deleteOne: function(request, action) {
            Api.post({
                uri: 'user/post/delete',
                data: {
                    post_id: request.post.id
                },
                statusCode: {
                    200: function()
                    {
                        AppDispatcher.serverAction({
                            type: action,
                            post: request.post
                        });
                    }
                }
            });
        }
    }
};