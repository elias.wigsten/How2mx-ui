function Request() {
    var request = {
        url: 'http://localhost:8888/How2mx/project/API/public/',
        method: null,
        data: null,
        statusCode: null
    };

    this.setRoute = function(uri)
    {
        request.url += uri;

        return this;
    };

    this.setMethod = function(method)
    {
        request.method = method;

        return this;
    };

    this.setData = function(data)
    {
        request.data = data;

        this.setContentType(data);

        return this;
    };

    this.setContentType = function(items)
    {
        for (var i in items) {
            if (items.hasOwnProperty(i)) {
                var item = items[i];

                if (item != null && item.hasOwnProperty('preview')) {
                    request.ContentType = false;

                    break;
                }
            }
        }
    };

    this.setStatusCodes = function(callbacks)
    {
        request.statusCode = callbacks;

        return this;
    };

    this.get = function()
    {
        return request;
    };

    return this;
}

module.exports = Request;