module.exports = {
    requests: [],

    add: function(request)
    {
        this.requests.push(request);
    },

    next: function()
    {
        return this.requests[0];
    },

    done: function()
    {
        this.requests.shift();
    },

    isEmpty: function()
    {
        return this.requests.length === 0;
    }
};