module.exports = {
    today: function()
    {
        return new Date().toISOString().slice(0, 10);
    }
};