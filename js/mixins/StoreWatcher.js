module.exports = {
    getInitialState: function()
    {
        return this.constructor.getState(this.props, this.state, this.context);
    },

    // Update component state when store changes
    storeChanged: function()
    {
        if (this.isMounted()) {
            this.setState(this.constructor.getState(this.props, this.state, this.context));
        }
    },

    // Add store listeners
    componentDidMount: function()
    {
        for (var i in this.constructor.stores) {
            if (this.constructor.stores.hasOwnProperty(i)) {
                var store = this.constructor.stores[i];

                store.addChangeListener(this.storeChanged, this);
            }
        }
    },

    // Remove store listeners
    componentWillUnmount: function()
    {
        for (var key in this.constructor.stores) {
            if (this.constructor.stores.hasOwnProperty(key)) {
                var store = this.constructor.stores[key];

                store.removeChangeListener(this.storeChanged, this);
            }
        }
    }
};