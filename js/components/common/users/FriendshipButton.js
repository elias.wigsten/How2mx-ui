var React = require('react');
var PropTypes = React.PropTypes;

// Templates
var DropDownButton = require('../buttons/DropDownButton');

// Actions
var UserActions = require('../../../actions/api/UserActions');

// Stores
var UserStore = global.UserStore;

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

module.exports = React.createClass({
    contextTypes: {
        user: PropTypes.object.isRequired
    },

    PropTypes: {
        user: PropTypes.object.isRequired
    },

    render: function()
    {
        if (this.props.user.friendship === undefined || !this.context.user.id) {
            return false;
        }

        return this.makeButton();
    },

    makeButton: function()
    {
        var friendship = this.props.user.friendship;

        if (friendship !== null) {
            if (friendship.status === 0) {
                if (friendship.recipient == this.context.user.id) {
                    var actions = {
                        Accept: this.acceptFriend,
                        Decline: this.removeFriend
                    };

                    return <DropDownButton title="Respond" actions={actions}/>;
                }
                else {
                    return <button className="friendship_button" onClick={this.removeFriend}>Cancel</button>;
                }
            } else if (friendship.status == 1) {
                return <button className="friendship_button" onClick={this.removeFriend}>Remove friend</button>;
            }
        }

        return <button className="friendship_button" onClick={this.addFriend}>Add friend</button>;
    },

    acceptFriend: function()
    {
        UserActions.acceptFriend(this.props.user);
    },

    addFriend: function()
    {
        UserActions.addFriend(this.props.user);
    },

    removeFriend: function()
    {
        UserActions.removeFriend(this.props.user);
    }
});