var React = require('react');
var PropTypes = React.PropTypes;

// Templates
var ProfileImage = require('./ProfileImage');
var Link = require('react-router').Link;

module.exports = React.createClass({
    PropTypes: {
        user: PropTypes.object.isRequired,
        created_at: PropTypes.string
    },

    render: function()
    {
        var created_at;

        if (this.props.created_at !== null) {
            created_at = <p>{this.props.created_at}</p>;
        }

        return (
            <div>
                <ProfileImage url={this.props.user.profile_img} />
                <h3><Link className="profile_link" to={`user/${this.props.user.id}`}>{this.props.user.display_name}</Link></h3>
                {created_at}
                {this.props.user.description}
            </div>
        );
    }
});
