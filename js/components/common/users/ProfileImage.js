var React = require('react');
var PropTypes = React.PropTypes;

// Stores
var ConfigStore = require('../../../stores/ConfigStore');

module.exports = React.createClass({
    PropTypes: {
        url: PropTypes.string.isRequired
    },

    shouldComponentUpdate: function(nextProps)
    {
        return this.props.url !== nextProps.url;
    },

    getInitialState: function()
    {
        var uri = "defaultProfileImage.png";

        if (this.props.url !== null) {
                uri = this.props.url
        }

        return {
            url: ConfigStore.storage.url.pages() + uri
        };
    },

    render: function()
    {
        return (
            <img className="smallProfileImg" src={this.state.url}/>
        );
    }
});