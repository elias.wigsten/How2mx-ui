var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropType: {
        size: PropTypes.string,
        title: PropTypes.string.isRequired,
        actions: PropTypes.object.isRequired
    },

    getInitialState: function()
    {
        return {
            active: false
        };
    },

    render: function()
    {
        var actions = this.getActions();

        return (
            <div className="friendship_button_wrap">
                <button className="drop_down_button" onClick={this.toggle}>{this.props.title}</button>
                {actions}
            </div>
        );
    },

    toggle: function()
    {
        this.setState({
            active: this.state.active !== true
        });
    },

    getActions: function()
    {
        var actions = [];
        var key = 0;

        if (this.state.active) {
            for (var action in this.props.actions) {
                if (this.props.actions.hasOwnProperty(action)) {
                    actions.push(<li key={key} onClick={this.props.actions[action]}>{action}</li>);

                    key++;
                }
            }
            return <div className="drop_down_button_actions"><ul>{actions}</ul></div>;
        }
    }
});
