var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    contextTypes: {
        isAdmin: PropTypes.bool.isRequired,
        edit: PropTypes.bool.isRequired
    },

    PropTypes: {
        onSelect: PropTypes.func.isRequired,
        selected: PropTypes.bool.isRequired
    },

    shouldComponentUpdate: function(nextProps, nextState, nextContext)
    {
        return this.props.selected !== nextProps.selected ||
                this.context.isAdmin !== nextContext.isAdmin ||
                this.context.edit !== nextContext.edit;
    },

    render: function()
    {
        if (!this.context.isAdmin || !this.context.edit) {
            return false;
        }

        return (
            <input type='checkbox' onChange={this.onSelect} checked={this.props.selected} />
        );
    },

    onSelect: function()
    {
        this.props.onSelect(!this.props.selected);
    }
});