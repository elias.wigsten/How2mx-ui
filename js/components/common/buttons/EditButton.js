var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    contextTypes: {
        isAdmin: PropTypes.bool.isRequired,
        edit: PropTypes.bool.isRequired
    },

    PropTypes: {
        onClick: PropTypes.func.isRequired,
        disabled: PropTypes.bool.isRequired
    },

    render: function()
    {
        if (!this.context.isAdmin) {
            return false;
        }

        return (
            <button onClick={this.props.onClick} className='btn_save' disabled={this.props.disabled}>
                {this.context.edit ? 'DONE' : 'EDIT'}
            </button>
        );
    }
});