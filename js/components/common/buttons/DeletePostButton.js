var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        deletable: PropTypes.object.isRequired,
        onDelete: PropTypes.func.isRequired
    },

    render: function()
    {
        if (!this.props.deletable) {
            return false;
        }

        return <i className="fa fa-trash-o" onClick={this.props.onDelete} />;
    }
});