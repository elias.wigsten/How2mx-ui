var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        onDelete: PropTypes.func.isRequired,
        count: PropTypes.number.isRequired
    },

    render: function()
    {
        if (this.props.count === 0) {
            return false;
        }

        return (
            <button onClick={this.props.onDelete} className='btn_delete'>DELETE</button>
        );
    }
});