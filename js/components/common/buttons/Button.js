var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        icon: PropTypes.string,
        _onClick: PropTypes.func
    },

    render: function()
    {
        return <i className={this.props.icon} onClick={this.props._onClick} />;
    }
});