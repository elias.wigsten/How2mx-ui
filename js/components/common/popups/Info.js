var React = require('react');
var PropTypes = React.PropTypes;

var PopupTemplate = require('./Template');

module.exports = React.createClass({
    PropTypes: {
        title:      PropTypes.string.isRequired,
        message:    PropTypes.string.isRequired,
        onClose:    PropTypes.func.isRequired
    },

    render: function()
    {
        return (
            <PopupTemplate onClose={this.props.onClose}>
                <h3 id="popup_info_title">{this.props.title}</h3>
                <p id="popup_info_message">{this.props.message}</p>
                <button onClick={this.props.onClose}>OK</button>
            </PopupTemplate>
        );
    }
});