var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        onClose: PropTypes.func
    },

    render: function()
    {
        var closeButton = this.props.onClose ? <div onClick={this.onClose} id="close_popup">x</div> : false;

        return (
            <div id="popup_wrapper">
                <div id="popup_bg" onClick={this.onClose}></div>
                <div id="popup_box">
                    {closeButton}
                    {this.props.children}
                </div>
            </div>
        );
    },

    onClose: function()
    {
        if (this.props.onClose) {
            this.props.onClose();
        }
    }
});