var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        name: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired,
        checked: PropTypes.bool
    },

    getInitialState: function()
    {
        return {
            name: this.props.name,
            value: this.props.value,
            checked: this.props.checked
        };
    },

    componentWillReceiveProps: function(nextProps)
    {
        this.setState({
            checked: nextProps.checked
        });
    },

    render: function()
    {
        var RadioButton = this.makeRadioButton();

        return (
            <span className="radio_btn">
                {RadioButton}
                <label htmlFor={this.props.children}>{this.props.children}</label>
            </span>
        );
    },

    makeRadioButton: function()
    {
        if (this.state.checked) {
            return <input onChange={this.props.onChange} type="radio" id={this.props.children} name={this.state.name} value={this.state.value} checked />;
        }

        return <input onChange={this.props.onChange} type="radio" id={this.props.children} name={this.state.name} value={this.state.value} />;
    }
});