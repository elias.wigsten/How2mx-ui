var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    propTypes: {
        name: PropTypes.string.isRequired,
        first: PropTypes.number.isRequired,
        last: PropTypes.number.isRequired,
        selected: PropTypes.string.isRequired
    },

    getInitialState: function()
    {
        return {
            selected: this.props.selected
        };
    },

    render: function()
    {
        var options = this.makeOptions();

        return (
            <select onChange={this.props._onChange} name={this.props.name} value={this.state.selected}>
                {options}
            </select>
        );
    },

    componentWillReceiveProps: function(nextProps)
    {
        this.setState({
            selected: nextProps.selected
        });
    },

    makeOptions: function()
    {
        if (this.props.first < this.props.last) {
            return this.incremented();
        }
        else {
            return this.decremented();
        }
    },

    incremented: function()
    {
        var options = [];

        for (var i = this.props.first; i <= this.props.last; i++) {
            options.push(<option key={i} value={i}>{i}</option>);
        }

        return options;
    },

    decremented: function()
    {
        var options = [];

        for (var i = this.props.first; i >= this.props.last; i--) {
            options.push(<option key={i} value={i}>{i}</option>);
        }

        return options;
    }
});
