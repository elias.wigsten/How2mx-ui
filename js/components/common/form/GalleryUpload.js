var React = require('react');
var PropTypes = React.PropTypes;

var Dropzone = require('react-dropzone');
var FileListPreview = require('./FileListPreview');

module.exports = React.createClass({
    PropTypes: {
        onDrop: PropTypes.func.isRequired
    },

    render: function()
    {
        return (
            <Dropzone ref="dropzone" className="media_upload_area" onDrop={this.props.onDrop} disableClick={true}>
                <div className="dropzone_content">
                    <h1>Drop files or</h1>
                    <button onClick={this.browseFiles}>Browse files</button>
                </div>
            </Dropzone>
        );
    },

    browseFiles: function()
    {
        this.refs.dropzone.open();
    }
});