var React = require('react');
var Prop = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        inputErrors: Prop.array
    },

    render: function()
    {
        var errors = this.buildErrors();

        return (
            <ul id="input_errors">
                {errors}
            </ul>
        );
    },

    buildErrors: function()
    {
        var errors = [];

        for (var key in this.props.inputErrors) {
            if (this.props.inputErrors.hasOwnProperty(key)) {

                errors.push(<li key={key}>{this.props.inputErrors[key]}</li>);
            }
        }

        return errors;
    }
});