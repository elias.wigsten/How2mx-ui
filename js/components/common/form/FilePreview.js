var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        file: PropTypes.object.isRequired
    },

    render: function()
    {
        return (
            <div className="file_preview">
                <img src={this.props.file.preview} />
            </div>
        );
    }
});