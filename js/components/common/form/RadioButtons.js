// Modules
var React = require('react');
var PropTypes = React.PropTypes;

// Templates
var RadioButton = require('./RadioButton');

module.exports = React.createClass({
    PropTypes: {
        name: PropTypes.string.isRequired,
        values: PropTypes.array.isRequired,
        checked: PropTypes.string.isRequired
    },

    getInitialState: function()
    {
        return {
            name: this.props.name,
            values: this.props.values,
            checked: this.props.checked
        };
    },

    componentWillReceiveProps: function(nextProps)
    {
        this.setState({
            checked: nextProps.checked
        });
    },

    render: function()
    {
        var RadioButtons = this.makeRadioButtons();

        return (
            <div className="radio_buttons">{RadioButtons}</div>
        );
    },

    makeRadioButtons: function()
    {
        var RadioButtons = [];

        for (var i = 0; i < this.state.values.length; i++) {
            var checked = this.isChecked(i);

            RadioButtons.push(<RadioButton onChange={this.props.onChange} key={i} name={this.state.name} value={i} checked={checked}>{this.state.values[i]}</RadioButton>);
        }

        return RadioButtons;
    },

    isChecked: function(i)
    {
        return i == this.state.checked;
    }
});