var React = require('react');

var InputErrors = require('./InputErrors');

module.exports = React.createClass({
    render: function()
    {
        if (this.props.errors === null) {
            return false;
        }

        var errors = this.buildErrors();

        return (
            <ul id="form_errors">
                {errors}
            </ul>
        );
    },

    buildErrors: function()
    {
        var errors = [];
        var key = 0;

        for (var inputKey in this.props.errors) {
            if (this.props.errors.hasOwnProperty(inputKey)) {
                var inputErrors = this.props.errors[inputKey];

                errors.push(<li key={key}>{inputKey}: <InputErrors inputErrors={inputErrors} /></li>);
            }

            key++;
        }

        return errors;
    }
});