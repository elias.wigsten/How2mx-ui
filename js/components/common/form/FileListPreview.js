var React = require('react');
var PropTypes = React.PropTypes;

// Templates
var FilePreview = require('./FilePreview');

module.exports = React.createClass({
    PropTypes: {
        files: PropTypes.array.isRequired
    },

    render: function()
    {
        var previewFiles = this.createPreviewList();

        if (previewFiles.length == 0) {
            return false;
        }

        return (
            <div className="upload_file_preview_list">
                <ul>
                    {previewFiles}
                </ul>
            </div>
        );
    },

    createPreviewList: function()
    {
        var files = [];

        this.props.files.forEach( function(file, i) {
            files.push(<li key={i}><FilePreview file={file} /></li>);
        });

        return files;
    }
});