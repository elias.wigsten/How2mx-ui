var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        className: PropTypes.string,
        id: PropTypes.string
    },

    render: function()
    {
        return (
            <table id={this.props.id} className={this.props.className}>
                <tbody>
                    {this.props.children}
                </tbody>
            </table>
        );
    }
});