var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    PropTypes: {
        className: PropTypes.string,
        id: PropTypes.string
    },

    render: function()
    {
        return (
            <tr>
                <td id={this.props.id} className={this.props.className}>
                    {this.props.children}
                </td>
            </tr>
        );
    }
});