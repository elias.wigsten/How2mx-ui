var React = require('react');
var PropTypes = React.PropTypes;

// Helpers
var Dropzone = require('react-dropzone');

module.exports = React.createClass({
    PropTypes: {
        onDrop: PropTypes.func.isRequired
    },

    render: function()
    {
        return (
            <li className='gallery_box_wrapper gallery_new'>
                <div className="gallery_box gallery_new">
                    <Dropzone ref="dropzone" className="dropzone" onDrop={this.props.onDrop}>
                        <h3>Upload Media</h3>
                    </Dropzone>
                </div>
            </li>
        );
    }
});