var React = require('react');
var PropTypes = React.PropTypes;
var Immutable = require('immutable');

// Templates
var GalleryItem = require('./GalleryItem');
var UploadMedia = require('./UploadMedia');
var GalleryTitle = require('./GalleryTitle');
var EditButton = require('../buttons/EditButton');
var DeleteButton = require('../buttons/DeleteItemsButton');
var FileUploadSpinner = require('./FileUploadSpinner');
var Header = require('./GalleryHeader');

// Actions
var Api = require('../../../actions/Api');
var GalleryActions = require('../../../actions/GalleryActions');
var UserActions = require('../../../actions/api/UserActions');

module.exports = React.createClass({
    contextTypes: {
        isAdmin: PropTypes.bool.isRequired
    },

    childContextTypes: {
        edit: PropTypes.bool
    },

    getChildContext: function()
    {
        return {
            edit: this.state.edit
        };
    },

    PropTypes: {
        gallery: PropTypes.object.isRequired
    },

    getInitialState: function()
    {
        return {
            edit: this.context.isAdmin && this.props.gallery.title === 'Untitled',
            filesToUpload: this.props.gallery.files_to_upload,
            changedItems: new Immutable.OrderedMap(),
            selectedItems: new Immutable.OrderedMap()
        };
    },

    isUploading: function()
    {
        return this.state.filesToUpload !== undefined && this.state.filesToUpload.length > 0;
    },

    componentWillUpdate: function(nextProps, nextState)
    {
        var numberOfUploadedFiles = nextProps.gallery.items.all.size - this.props.gallery.items.all.size;

        if (numberOfUploadedFiles > 0 && nextState.filesToUpload && nextState.filesToUpload.length >= numberOfUploadedFiles) {
            nextState.filesToUpload.splice(0, numberOfUploadedFiles);
        }
    },

    render: function()
    {
        var items = this.createGalleryItems();

        var more;

        if (this.props.gallery.canRefresh) {
            more = <button onClick={this.refresh}>More</button>;
        }

        return (
            <div id="page_content" className="gallery">
                <Header title={this.props.gallery.title}
                onSave={this.saveTitle}
                onReset={this.resetTitle}
                onChange={this.titleChanged}>
                    <DeleteButton onDelete={this.onDeleteItems}
                        count={this.state.selectedItems.size} />
                    <EditButton onClick={this.toggleEdit}
                        disabled={!this.props.gallery.titleIsValid() || this.isUploading()} />
                </Header>
                {items}
                {more}
            </div>
        );
    },

    addChangesTo: function(item)
    {
        var changedItem = this.state.changedItems.get(item.id);
        var selectedItem = this.state.selectedItems.get(item.id);

        if (changedItem) {
            if (item.original_description === changedItem.description) {
                this.state.changedItems = this.state.changedItems.remove(item.id);
            } else {
                item = changedItem;
            }
        }

        item = selectedItem ? item.set('selected', true) : item;

        return item;
    },

    createGalleryItems: function()
    {
        var galleryItems = this.props.gallery.items.all;
        var items = [];

        if (this.context.isAdmin) {
            items.push(<UploadMedia key={0} onDrop={this.onDrop} />);
        }

        if (this.state.filesToUpload) {
            this.state.filesToUpload.forEach(function (file) {
                items.push(<FileUploadSpinner key={items.length}
                                onUpload={this.props.children.onUpload}
                                file={file} />);
            }.bind(this));
        }

        if (galleryItems.size > 0) {
            galleryItems.forEach(function(item) {
                item = this.addChangesTo(item);

                items.push(<GalleryItem key={items.length}
                                item={item}
                                saveDescription={this.props.children.saveItemDescription}
                                onChange={this.itemChanged}
                                onSelect={this.onItemSelect} />);
            }.bind(this));
        }

        if (items.length > 0) {
            return (
                <ul className="grid_list">
                    {items}
                </ul>
            );
        }

        return <h1>No media :(</h1>;
    },

    refresh: function()
    {
        if (this.props.gallery.canRefresh) {
            this.props.children.onRefresh();
        }
    },

    onDeleteItems: function()
    {
        var items = [];

        this.props.gallery.items.all.some(function(item) {
            if (this.state.selectedItems.get(item.id)) {
                items.push(item);

                if (items.length === this.state.selectedItems.size) {
                    return true;
                }
            }
        }.bind(this));

        if (items.length > 0) {
            this.props.children.deleteItems(items);
            // GalleryActions.deleteItems(items);
        }

        this.state.selectedItems = this.state.selectedItems.clear();
    },

    onItemSelect: function(item)
    {
        if (item.selected) {
            this.state.selectedItems = this.state.selectedItems.set(item.id, true);
        } else {
            this.state.selectedItems = this.state.selectedItems.remove(item.id);
        }

        var updatedGallery = this.props.gallery.changeItem(item);
        this.props.children.onChange(updatedGallery);
    },

    itemChanged: function(item)
    {
        if (item.changed()) {
            this.state.changedItems = this.state.changedItems.set(item.id, item);
        } else {
            if (this.state.changedItems.get(item.id)) {
                this.state.changedItems = this.state.changedItems.remove(item.id);
            }
        }

        var updatedGallery = this.props.gallery.changeItem(item);
        this.updateGallery(updatedGallery);
    },

    saveTitle: function()
    {
        if (this.props.gallery.titleChanged()) {
            this.props.children.saveTitle();
        }
    },

    titleChanged: function(title)
    {
        var updatedGallery = this.props.gallery.set('title', title);
        this.updateGallery(updatedGallery);
    },

    resetTitle: function()
    {
        var updatedGallery = this.props.gallery.set('title', this.props.gallery.get('original_title'));

        this.updateGallery(updatedGallery);
    },

    onDrop: function(files)
    {
        this.setState({
            edit: true,
            filesToUpload: files
        });
    },

    updateGallery: function(updatedGallery)
    {
        this.props.children.onChange(updatedGallery);
        this.setState({
            edit: true
        });
    },

    /*onToggleEdit: function()
    {
        if (!this.props.gallery.get('saved')) {
            this.props.onSave(this.props.gallery.set('saved', true));
            this.toggleEdit();
        } else {
            this.toggleEdit();
        }
    },*/

    toggleEdit: function()
    {
        this.setState({
            edit: !this.state.edit
        });
    }
});