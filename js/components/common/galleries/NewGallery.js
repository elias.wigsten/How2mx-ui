var React = require('react');
var PropTypes = React.PropTypes;

// Helpers
var Dropzone = require('react-dropzone');

module.exports = React.createClass({
    PropTypes: {
        onNewGallery: PropTypes.func.isRequired
    },

    render: function()
    {
        return (
            <div className="gallery_box_wrapper">
                <Dropzone ref="dropzone" className="gallery_box gallery_new" onDrop={this.props.onNewGallery} onClick={this.props.onNewGallery}>
                    <h3>New Gallery</h3>
                </Dropzone>
            </div>
        );
    }
});