var React = require('react');
var PropTypes = React.PropTypes;

// Constants
var KeyCode = require('../../../constants/KeyCode');

module.exports = React.createClass({
    contextTypes: {
        edit: PropTypes.bool.isRequired,
        isAdmin: PropTypes.bool.isRequired
    },

    PropTypes: {
        title: PropTypes.string.isRequired,
        onSave: PropTypes.func.isRequired,
        onChange: PropTypes.func.isRequired,
        onReset: PropTypes.func.isRequired
    },

    getInitialState: function()
    {
        return {
            isValid: this.validate(this.props.title)
        };
    },

    componentDidUpdate: function()
    {
        if (!this.state.isValid) {
            this.refs.titleInput.focus();
        }
    },

    componentWillUpdate: function(nextProps, nextState)
    {
        nextState.isValid = this.validate(nextProps.title);
    },

    validate: function(title)
    {
        return title.length > 0;
    },

    render: function()
    {
        var className = this.createClassName();
        var title;
        var description = "This is a static description I made in order to see how a longer description will look in the UI";

        if (this.context.isAdmin && this.context.edit) {
            title = (
                <input ref='titleInput'
                className={className}
                onChange={this.onChange}
                onBlur={this.onBlur}
                onKeyUp={this.onKeyUp}
                type='text'
                placeholder='Title'
                value={this.props.title}/>
            );
        } else {
            title = (
                <h1 className={className}>{this.props.title}</h1>
            );
        }

        return (
            <div className="box header">
                {title}
                <p>{description}</p>
                <p>X minutes ago</p>
                <div className="admin_btns">
                    {this.props.children}
                </div>
            </div>
        );
    },

    createClassName: function()
    {
        var className = 'title';

        return this.state.isValid ? className : className += ' validation_failed';
    },

    onKeyUp: function(e)
    {
        if (e.keyCode === KeyCode.ESCAPE) {
            this.props.onReset();
        } else if (e.keyCode === KeyCode.ENTER) {
            e.target.blur();
        }
    },

    onBlur: function()
    {
        if (!this.state.isValid) {
            this.refs.titleInput.focus();
        } else {
            this.props.onSave();
        }
    },

    onChange: function(e)
    {
        if (e.target.value.length < 32) {
            this.props.onChange(e.target.value);
        }
    }
});