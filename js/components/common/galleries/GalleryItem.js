var React = require('react');
var PropTypes = React.PropTypes;
var $ = require('jquery');

// Templates
var ItemDescription = require('./GalleryItemDescription');
var ItemSelectButton = require('../buttons/ItemSelectButton');

// Actions
var Api = require('../../../actions/Api');

// Utils
var MediaUtils = require('../../../utils/MediaUtils');

module.exports = React.createClass({
    contextTypes: {
        isAdmin: PropTypes.bool.isRequired,
        edit: PropTypes.bool.isRequired
    },

    PropTypes: {
        item: PropTypes.object.isRequired,
        onChange: PropTypes.func.isRequired,
        onSelect: PropTypes.func.isRequired,
        saveDescription: PropTypes.func.isRequired
    },

    shouldComponentUpdate: function(nextProps, nextState, nextContext)
    {
        return this.props.item !== nextProps.item ||
            this.context.isAdmin !== nextContext.isAdmin ||
            this.context.edit !== nextContext.edit;
    },

    render: function()
    {
        var src = MediaUtils.getPreviewFor(this.props.item);

        return (
            <li className='gallery_box_wrapper'>
                <div className='gallery_box gallery_item'>
                    <img src={src} />
                    <ItemSelectButton onSelect={this.onSelect}
                        selected={this.props.item.selected} />
                    <ItemDescription onChange={this.descriptionChanged}
                        description={this.props.item.description}
                        onSave={this.saveDescription} />
                </div>
            </li>
        );
    },

    onSelect: function(selected)
    {
        var updatedItem = this.props.item.set('selected', selected);

        this.props.onSelect(updatedItem);
    },

    descriptionChanged: function(e)
    {
        var updatedItem = this.props.item.set('description', e.target.value);

        this.props.onChange(updatedItem);
    },

    saveDescription: function()
    {
        if (this.props.item.descriptionChanged()) {
            this.props.saveDescription(this.props.item);
        }
    }
});