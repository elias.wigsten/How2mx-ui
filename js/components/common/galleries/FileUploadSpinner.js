var React = require('react');
var PropTypes = React.PropTypes;
var $ = require('jquery');

// Actions
var Api = require('../../../actions/Api');

module.exports = React.createClass({
    PropTypes: {
        file: PropTypes.object.isRequired,
        onUpload: PropTypes.func.isRequired
    },

    componentDidMount: function()
    {
        this.props.onUpload(this.props.file);
    },

    render: function()
    {
        return (
            <li className='gallery_box_wrapper'>
                <div className='gallery_box gallery_item'>
                    <div className="sk-circle load-spinner">
                        <div className="sk-circle1 sk-child"></div>
                        <div className="sk-circle2 sk-child"></div>
                        <div className="sk-circle3 sk-child"></div>
                        <div className="sk-circle4 sk-child"></div>
                        <div className="sk-circle5 sk-child"></div>
                        <div className="sk-circle6 sk-child"></div>
                        <div className="sk-circle7 sk-child"></div>
                        <div className="sk-circle8 sk-child"></div>
                        <div className="sk-circle9 sk-child"></div>
                        <div className="sk-circle10 sk-child"></div>
                        <div className="sk-circle11 sk-child"></div>
                        <div className="sk-circle12 sk-child"></div>
                    </div>
                </div>
            </li>
        );
    }
});