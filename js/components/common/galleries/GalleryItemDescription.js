var React = require('react');
var PropTypes = React.PropTypes;

module.exports = React.createClass({
    contextTypes: {
        isAdmin: PropTypes.bool.isRequired,
        edit: PropTypes.bool.isRequired
    },

    PropTypes: {
        description: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
        onSave: PropTypes.func.isRequired
    },

    shouldComponentUpdate: function(nextProps, nextState, nextContext)
    {
        return this.props.description !== nextProps.description ||
                this.context.isAdmin !== nextContext.isAdmin ||
                this.context.edit !== nextContext.edit;
    },

    render: function()
    {
        var description;

        if (!this.context.edit && !this.props.description) {
            return false;
        }

        if (this.context.edit && this.context.isAdmin) {
                description = (
                    <textarea onChange={this.props.onChange}
                        placeholder='Description'
                        onBlur={this.props.onSave}
                        value={this.props.description} />
                );
        } else {
            description = (
                <h3>{this.props.description}</h3>
            );
        }

        return (
            <div className='gallery_description'>
                {description}
            </div>
        );
    }
});