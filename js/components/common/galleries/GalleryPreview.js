var React = require('react');
var PropTypes = React.PropTypes;
var Link = require('react-router').Link;

// Utils
var MediaUtils = require('../../../utils/MediaUtils');

module.exports = React.createClass({
    contextTypes: {
        user: PropTypes.object.isRequired,
        isAdmin: PropTypes.bool.isRequired
    },

    PropTypes: {
        gallery: PropTypes.object.isRequired
    },

    render: function()
    {
        var src = MediaUtils.getPreview(this.props.gallery.page_id, this.props.gallery.id, this.props.gallery.preview);

        return (
            <li className='gallery_box_wrapper'>
                <Link to={`user/${this.props.gallery.page_id}/gallery/${this.props.gallery.id}`} >
                    <div className='gallery_box gallery_item'>
                        <img src={src} />
                        <h5>{this.props.gallery.title}</h5>
                    </div>
                </Link>
            </li>
        );
    }
});