var React = require('react');
var PropTypes = React.PropTypes;

// Constants
var KeyCode = require('../../../constants/KeyCode');

module.exports = React.createClass({
    contextTypes: {
        edit: PropTypes.bool.isRequired,
        isAdmin: PropTypes.bool.isRequired
    },

    PropTypes: {
        title: PropTypes.string.isRequired,
        onSave: PropTypes.func.isRequired,
        onChange: PropTypes.func.isRequired,
        onReset: PropTypes.func.isRequired
    },

    getInitialState: function()
    {
        return {
            isValid: this.validate(this.props.title)
        };
    },

    componentDidUpdate: function()
    {
        if (!this.state.isValid) {
            this.refs.titleInput.focus();
        }
    },

    componentWillUpdate: function(nextProps, nextState)
    {
        nextState.isValid = this.validate(nextProps.title);
    },

    validate: function(title)
    {
        return title.length > 0;
    },

    render: function()
    {
        var className = this.createClassName();
        var title;

        if (this.context.isAdmin && this.context.edit) {
            title = (
                <input ref='titleInput'
                        onChange={this.onChange}
                        onBlur={this.onBlur}
                        onKeyUp={this.onKeyUp}
                        type='text'
                        placeholder='Title'
                        value={this.props.title}/>
            );
        } else {
            title = (
                <h3>{this.props.title}</h3>
            );
        }

        return (
            <div className={className}>
                {title}
            </div>
        );
    },

    createClassName: function()
    {
        var className = 'gallery_title';

        return this.state.isValid ? className : className += ' validation_failed';
    },

    onKeyUp: function(e)
    {
        if (e.keyCode === KeyCode.ESCAPE) {
            this.props.onReset();
        } else if (e.keyCode === KeyCode.ENTER) {
            e.target.blur();
        }
    },

    onBlur: function()
    {
        if (!this.state.isValid) {
            this.refs.titleInput.focus();
        } else {
            this.props.onSave();
        }
    },

    onChange: function(e)
    {
        if (e.target.value.length < 32) {
            this.props.onChange(e.target.value);
        }
    }
});