var React = require('react');
var PropTypes = React.PropTypes;

// Actions
var Api = require('../../../actions/Api');

module.exports = React.createClass({
    PropTypes: {
        recipient: PropTypes.object.isRequired
    },

    ContextTypes: {
        user: PropTypes.object
    },

    getInitialState: function()
    {
        return {
            message: ""
        };
    },

    shouldComponentUpdate: function(nextProps, nextState)
    {
        return this.state.message !== nextState.message;
    },

    render: function()
    {
        return (
            <form id="post_form" onSubmit={this.onPost}>
                <textarea onChange={this.onChange} placeholder="Write a message" rows="3" value={this.state.message}>
                </textarea>
                <input type="submit" value="Post"/>
            </form>
        );
    },

    onChange: function(e)
    {
        this.setState({
            message: e.target.value
        });
    },

    onPost: function(e)
    {
        e.preventDefault();

        if (this.state.message.length > 0) {
            Api.user.feed.create(this.props.recipient, this.state.message);

            this.setState({
                message: ""
            })
        }
    }
});