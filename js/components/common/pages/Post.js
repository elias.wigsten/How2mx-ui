var React = require('react');
var PropTypes = React.PropTypes;

// Actions
var Api = require('../../../actions/Api');

// Templates
var PagePreviewCard = require('./PagePreviewCard');
var PostContent = require('./PostContent');
var DeletePostButton = require('../buttons/DeletePostButton');

module.exports = React.createClass({
    PropTypes: {
        post: PropTypes.object.isRequired
    },

    render: function()
    {
        var post = this.props.post;

        return (
            <div className="post box">
                <PagePreviewCard page={post.author} createdAt={post.created_at} >
                    <DeletePostButton deletable={post.deletable} onDelete={this.onDelete} />
                </PagePreviewCard>
                <PostContent message={post.message} />
            </div>
        );
    },

    onDelete: function()
    {
        Api.user.feed.deleteOne(this.props.post);
    }
});