var React = require('react');
var PropTypes = React.PropTypes;

// Templates
var ProfileImage = require('./ProfileImage');
var Link = require('react-router').Link;

// Utils
var TimestampUtils = require('../../../utils/Timestamps');

module.exports = React.createClass({
    PropTypes: {
        page: PropTypes.object.isRequired,
        createdAt: PropTypes.string
    },

    getInitialState: function()
    {
        return {
            createdAt: null
        }
    },

    componentWillMount: function()
    {
        if (this.props.createdAt) {
            this.updateCreatedAt();
        }
    },

    render: function()
    {
        var createdAt = this.getCreatedAt();

        return (
            <div className="page_preview_card">
                <ProfileImage image={this.props.page.getProfileImage()} />
                <div className="info">
                    <Link className="profile_link" to={`user/${this.props.page.id}`}>{this.props.page.display_name}</Link>
                    {createdAt}
                </div>
                <div className="actions">
                    {this.props.children}
                </div>
            </div>
        );
    },

    getCreatedAt: function()
    {
        var createdAt = this.state.createdAt;

        if (createdAt !== null) {
            return <p className="post_created">{createdAt.diff} {TimestampUtils.intervalToText(createdAt.interval)} ago</p>;
        }

        return false;
    },

    updateCreatedAt: function()
    {
        var createdAt = TimestampUtils.timePassedSince(this.props.createdAt);

        setTimeout(function() {
            if (this.isMounted()) {
                this.updateCreatedAt();
            }
        }.bind(this), createdAt.interval);

        this.setState({
            createdAt: createdAt
        });
    }
});