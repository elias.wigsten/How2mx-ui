var React = require('react');
var PropTypes = React.PropTypes;

// Stores
var ConfigStore = require('../../../stores/ConfigStore');

// Utils
var MediaUtils = require('../../../utils/MediaUtils');

module.exports = React.createClass({
    PropTypes: {
        image: PropTypes.object.isRequired
    },

    shouldComponentUpdate: function(nextProps)
    {
        return this.props.image !== nextProps.image;
    },

    render: function()
    {
        var url = MediaUtils.makeImage(this.props.image);

        return (
            <img className="smallProfileImg" src={url}/>
        );
    }
});
