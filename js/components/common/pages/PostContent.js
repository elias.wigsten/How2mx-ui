var React = require('react');
var PropTypes = React.PropTypes;

// Templates
var Post = require('./Post');

module.exports = React.createClass({
    PropTypes: {
        message: PropTypes.object.isRequired
    },

    render: function()
    {
        return this.getContentTemplate();
    },

    getContentTemplate: function()
    {
        return (
            <div className="post_content">
                <p className="message">{this.props.message}</p>
            </div>
        );
    }
});