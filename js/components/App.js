var React = require('react');
var PropTypes = React.PropTypes;

// Layout templates
var Header = require('./layout/header/Header');
var Footer = require('./layout/footer/Footer');
var Sidebar = require('./layout/sidebar/Sidebar');
var Popups = require('./layout/popups');

// Actions
var AuthActions = require('../actions/api/AuthActions');

// Stores
//var AuthStore = global.AuthStore;
var SelfStore = require('../stores/SelfStore');

// Mixins
var StoreWatcher = require('../mixins/StoreWatcher');

module.exports = React.createClass({
    mixins: [StoreWatcher],

    statics: {
        stores: [SelfStore],
        getState: function() {
            return {
                user: SelfStore.getUser()
            };
        }
    },

    childContextTypes: {
        user: PropTypes.object
    },

    getChildContext: function() {
        return {
            user: this.state.user
        }
    },

    componentDidMount: function()
    {
        AuthActions.remembered();
    },

    render: function()
    {
        if (!this.state.user) {
            return false;
        }

        return (
            <div id="app_wrap" key={this.state.user.id}>
                <Header />
                <div className="centered" id="page_wrap">
                    <div id="page">
                        {this.props.children}
                    </div>
                    <Sidebar />
                </div>
                <Footer />
                <Popups />
            </div>
        );
    }
});