var React = require('react');

module.exports = React.createClass({
    render: function()
    {
        return (
            <div id="page">
                <div className="box">
                    <h1>this is the home page!</h1>
                </div>
                <div className="box">
                    <p>Welcome!</p>
                </div>
            </div>
        );
    }
});