var React = require('react');
var PropTypes = React.PropTypes;

// Stores
var GalleryStore = require('../../../stores/GalleryStore');

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

// Actions
var GalleryActions = require('../../../actions/GalleryActions');

module.exports = React.createClass({
    contextTypes: {
        user: PropTypes.object.isRequired,
        isAdmin: PropTypes.bool.isRequired
    },

    PropTypes: {
        user: PropTypes.object.isRequired
    },

    mixins: [StoreWatcher],

    statics: {
        stores: [GalleryStore],
        getState: function(props, state, context)
        {
            if (context.isAdmin) {
                return {
                    gallery: GalleryStore.getNewGallery(props.params.page_id)
                };
            }
        }
    },

    componentDidMount: function()
    {
        if (this.context.isAdmin) {
            GalleryActions.createUserGallery();
        } else {
            console.log('404 page');
        }
    },

    shouldComponentUpdate: function(nextProps, nextState)
    {
        if (nextState.gallery && nextState.gallery.id) {
            var uri = 'user/' + nextState.gallery.page_id + '/gallery/' + nextState.gallery.id;
            this.props.history.push(uri);
        }

        return false;
    },

    render: function()
    {
        return (
            <div id="page_content"></div>
        );
    }
});