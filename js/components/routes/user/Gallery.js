var React = require('react');
var PropTypes = React.PropTypes;
var Immutable = require('immutable');

// Stores
/*var SelfStore = global.SelfStore;
var UserStore = global.UserStore;*/
var GalleryStore = require('../../../stores/GalleryStore');

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

// Actions
var Api = require('../../../actions/Api');
var UserActions = require('../../../actions/api/UserActions');
var GalleryActions = require('../../../actions/GalleryActions');

// Templates
var Gallery = require('../../common/galleries/Gallery');

module.exports = React.createClass({
    contextTypes: {
        user: PropTypes.object.isRequired
    },

    PropTypes: {
        user: PropTypes.object.isRequired
    },

    mixins: [StoreWatcher],

    statics: {
        stores: [GalleryStore],
        getState: function(props, state)
        {
            return {
                gallery: GalleryStore.getGallery(props.params.page_id, props.params.gallery_token),
                selectedItems: state ? state.selectedItems : new Immutable.OrderedMap()
            };
        }
    },

    shouldComponentUpdate: function(nextProps, nextState, nextContext)
    {
        if (nextState.gallery && nextState.gallery.shouldFetchNewItems() ||
            !this.context.user.id && nextContext.user.id) {
            Api.user.galleries.items.get(nextState.gallery);
        }

        return this.state.gallery !== nextState.gallery ||
            this.state.selectedItems !== nextState.selectedItems ||
            this.context.user !== nextContext.user ||
            (this.props.user && this.props.user !== nextProps.user);
    },

    componentWillUpdate: function(nextProps, nextState, nextContext)
    {
        /*nextState.gallery.items.all.forEach(function(item) {
            if (nextState.selectedItems.get(item.id)) {
                nextState.gallery = nextState.gallery.select(item);
            } else {
                if (item.selected) {
                    nextState.gallery = nextState.gallery.deselect(item);
                }
            }
        });*/
    },

    componentDidMount: function()
    {
        if (!this.state.gallery) {
            Api.user.galleries.getOne(this.props.params.page_id, this.props.params.gallery_token);
        } else {
            var files_to_upload = this.state.gallery.files_to_upload;

            if (!files_to_upload || files_to_upload.length === 0) {
                Api.user.galleries.items.get(this.state.gallery);
            }
        }
        //this.state.selectedItems = new Immutable.OrderedMap();
    },

    render: function()
    {
        if (this.state.gallery == null) {
            return false;
        }

        return (
            <Gallery gallery={this.state.gallery}>
                {{
                    onChange: this.onChange,
                    onRefresh: this.refresh,
                    saveTitle: this.saveTitle,
                    onUpload: this.upload,
                    saveItemDescription: this.saveItemDescription,
                    deleteItems: this.deleteItems
                }}
            </Gallery>
        );
    },

    onChange: function(gallery)
    {
        this.setState({
            gallery: gallery
        })
    },

    upload: function(file)
    {
        Api.user.galleries.items.upload(this.state.gallery, file);
    },

    refresh: function()
    {
        Api.user.galleries.items.next(this.state.gallery);
    },

    saveTitle: function()
    {
        Api.user.galleries.saveTitle(this.state.gallery);
    },

    saveItemDescription: function(item)
    {
        Api.user.galleries.items.saveDescription(item);
    },

    deleteItems: function(items)
    {
        GalleryActions.deleteItems(items);
    }
});