var React = require('react');
var PropTypes = React.PropTypes;

// Actions
var Api = require('../../../actions/Api');

// Templates
var WritePost = require('../../common/pages/WritePost');
var Post = require('../../common/pages/Post');
var Table = require('../../common/positioning/Table');
var Row = require('../../common/positioning/Row');

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

// Stores
var PostStore = require('../../../stores/PostStore');

module.exports = React.createClass({
    PropTypes: {
        user: PropTypes.object.isRequired
    },

    mixins: [StoreWatcher],

    statics: {
        stores: [PostStore],
        getState: function(props)
        {
            return {
                posts: PostStore.getPostsFor(props.params.page_id)
            };
        }
    },

    shouldComponentUpdate: function(nextProps, nextState)
    {
        //if (nextState.posts === undefined && this.props.user !== nextProps.user)
        if (nextState.posts === undefined || this.props.user !== nextProps.user) {
            Api.user.feed.get(nextProps.user.page_id, nextState.posts);
        }

        return this.state.posts != nextState.posts || this.props.user !== nextProps.user;
    },

    componentDidMount: function()
    {
        // this.state.posts === undefined && ( used earlier, figure out if it's really needed )
        if (this.props.user.page_id == this.props.params.page_id) {
            Api.user.feed.get(this.props.user.page_id, this.state.posts);
        }
    },

    render: function()
    {
        var postList = this.createPostList();

        return (
            <Table>
                <Row>
                    <WritePost recipient={this.props.user}/>
                </Row>
                <Row className='height_auto'>
                    <div id='page_content'>
                        {postList}
                    </div>
                </Row>
            </Table>
        );
    },

    createPostList: function()
    {
        var posts = this.state.posts;

        var postList = [];

        if (posts !== undefined) {
            if (posts.get('all').size > 0) {
                posts.get('all').forEach( function(post, i) {
                    postList.push(<Post key={i} post={post} />);
                });
            }
            else {
                postList.push(<h3 key={0}>No posts</h3>);
            }
        }

        return postList;
    }
});
