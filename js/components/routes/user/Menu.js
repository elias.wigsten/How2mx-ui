var React = require('react');
var Link = require('react-router').Link;

module.exports = React.createClass({
    render: function()
    {
        return (
            <ul id="user_menu">
                <li><Link to={`user/${this.props.page_id}`}>Stream</Link></li>
                <li><Link to={`user/${this.props.page_id}/galleries`}>Galleries</Link></li>
                <li><Link to={`user/${this.props.page_id}/friends`}>Friends</Link></li>
            </ul>
        );
    }
});