var React = require('react');
var $ = require('jquery');
var PropTypes = React.PropTypes;

// Template parts
var ProfileImage = require('../../common/users/ProfileImage');
var FriendshipButton = require('../../common/users/FriendshipButton');

module.exports = React.createClass({
    PropTypes: {
        user: PropTypes.object.isRequired
    },

    shouldComponentUpdate: function(nextProps)
    {
        return this.props.user !== nextProps.user
            || this.props.self_id !== nextProps.self_id;
    },

    render: function()
    {
        if (!this.props.user) {
            return false;
        }

        return (
            <div id="user_info_box" className="box">
                <h3>{this.props.user.f_name} {this.props.user.l_name}</h3>
                <ProfileImage url="defaultProfileImage.png" />
                <p>Age: {this.props.user.age}</p>
                <p>Gender: {this.props.user.gender}</p>
                <FriendshipButton user={this.props.user} />
            </div>
        );
    }
});