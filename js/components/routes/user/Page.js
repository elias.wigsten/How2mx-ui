var React = require('react');
var PropTypes = React.PropTypes;

// Templates
var UserInfoBox = require('./InfoBox');
var UserMenu = require('./Menu');
var Table = require('../../common/positioning/Table');
var Row = require('../../common/positioning/Row');

// Stores
var UserStore = global.UserStore;

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

// Actions
var UserActions = require('../../../actions/api/UserActions');

module.exports = React.createClass({
    contextTypes: {
        user: PropTypes.object.isRequired
    },

    childContextTypes: {
        isAdmin: PropTypes.bool
    },

    getChildContext: function()
    {
        return {
            isAdmin: this.context.user === this.state.user
        };
    },

    mixins: [StoreWatcher],

    statics: {
        stores: [UserStore],
        getState: function(props)
        {
            return {
                user: UserStore.getUser(props.params.page_id)
            };
        }
    },

    render: function()
    {
        var page_id = this.props.params.page_id;
        /** If the users page_id doesn't match the routes page_id
         *  We need to render an empty page in order for
         *  child pages to update properly
         */
        if (!this.state.user || this.state.user.page_id !== page_id) {
            return (
                <div id="page"></div>
            );
        }

        return (
            <Table>
                <Row>
                    <UserInfoBox user={this.state.user} />
                    <UserMenu page_id={page_id} />
                </Row>
                <Row className='height_auto'>
                    {React.cloneElement(this.props.children, { user: this.state.user })}
                </Row>
            </Table>
        );
    },

    shouldComponentUpdate: function(nextProps, nextState, nextContext)
    {
        var prevPageId = this.props.params.page_id;
        var nextPageId = nextProps.params.page_id;

        if (prevPageId !== nextPageId) {
            this.getUser(nextPageId);
        }

        var prevChildRoute = this.props.children.props.route.path;
        var nextChildRoute = nextProps.children.props.route.path;

        return  nextState.user !== this.state.user ||
                nextContext.user !== this.context.user ||
                nextState.isAdmin !== this.context.isAdmin ||
                prevChildRoute !== nextChildRoute ||
                prevPageId !== nextPageId;
    },

    /** If a user redirects to another user page while already visiting a user page
     *  The component will update but since the store haven't changed
     *  We need to manually trigger the storeChanged() method
     *  to update the state for the user page
     */
    componentDidUpdate: function()
    {
        if (!this.state.user || this.state.user.page_id !== this.props.params.page_id) {
            this.storeChanged();
        }
    },

    componentDidMount: function()
    {
        this.getUser(this.props.params.page_id);
    },

    getUser: function(page_id)
    {
        UserActions.fetchUser(page_id);
    }
});