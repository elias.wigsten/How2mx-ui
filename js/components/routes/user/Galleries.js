var React = require('react');
var PropTypes = React.PropTypes;
var History = require('react-router').History;

// Templates
var Upload = require('../../common/form/GalleryUpload');
var FileListPreview = require('../../common/form/FileListPreview');
var NewGallery = require('../../common/galleries/NewGallery');
var GalleryPreview = require('../../common/galleries/GalleryPreview');

// Stores
var SelfStore = global.SelfStore;
var UserStore = global.UserStore;
var GalleryStore = require('../../../stores/GalleryStore');

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

// Actions
var Api = require('../../../actions/Api');
var UserActions = require('../../../actions/api/UserActions');

module.exports = React.createClass({
    contextTypes: {
        user: PropTypes.object.isRequired,
        isAdmin: PropTypes.bool.isRequired
    },

    PropTypes: {
        user: PropTypes.object.isRequired
    },

    mixins: [StoreWatcher],

    statics: {
        stores: [GalleryStore],
        getState: function(props, state, context)
        {
            return {
                galleries: GalleryStore.getGalleries(props.params.page_id)
            };
        }
    },

    componentDidMount: function()
    {
        // TODO - ADD FETCHED_AT TIMESTAMP TO AS CONTROLLER
        if (this.state.galleries) {
            if (this.state.galleries.all.size <= 1 || this.state.galleries.shouldUpdate()) {
                Api.user.galleries.getAll(this.props.params.page_id);
            }
        } else {
            Api.user.galleries.getAll(this.props.params.page_id);
        }
    },

    shouldComponentUpdate: function(nextProps, nextState, nextContext)
    {
        // TODO - ADD FETCHED_AT TIMESTAMP TO AS CONTROLLER
        if (nextState.galleries && nextState.galleries.shouldUpdate()) {
            Api.user.galleries.getAll(nextProps.params.page_id);
        }

        return this.state.galleries !== nextState.galleries ||
            this.context.user !== nextContext.user ||
            this.context.isAdmin !== nextContext.isAdmin;
    },

    render: function()
    {
        var galleries = this.createGalleries();

        return (
            <div id="page_content">
                <ul className="grid_list">
                    {galleries}
                </ul>
            </div>
        );
    },

    createGalleries: function()
    {
        var galleries = [];

        if (this.context.isAdmin) {
            galleries.push(<NewGallery key={0} onNewGallery={this.createGallery} />);
        }

        if (this.state.galleries) {
            this.state.galleries.all.forEach(function(gallery) {
                if (!gallery.preview && gallery.items.all.size > 0) {
                    // TODO - change to first item ( ALSO IN BACKEND )
                    gallery = gallery.set('preview', gallery.items.all.last().src);
                }

                galleries.push(<GalleryPreview key={galleries.length} gallery={gallery} />);
            });
        }

        return galleries;
    },

    createGallery: function(files)
    {
        UserActions.createGallery(files);

        this.props.history.push("user/" + this.props.params.page_id + "/gallery/new");
    },

    onDrop: function(newFiles)
    {
        UserActions.uploadFiles(newFiles);
    }
});