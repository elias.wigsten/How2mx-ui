var React = require('react');
var PropTypes = React.PropTypes;

// Actions
var UserActions = require('../../../actions/api/UserActions');

// Templates
var UserInfoCard = require('../../common/users/UserInfoCard');

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

// Stores
var UserStore = global.UserStore;

module.exports = React.createClass({
    PropTypes: {
        user: PropTypes.object.isRequired
    },

    mixins: [StoreWatcher],

    statics: {
        stores: [UserStore],
        getState: function(props)
        {
            return {
                friends: UserStore.getFriendsFor(props.params.page_id)
            }
        }
    },

    shouldComponentUpdate: function(nextProps, nextState)
    {
        if (!nextState.friends) {
            UserActions.fetchFriendsFor(nextProps.user);
        }

        return this.state.friends !== nextState.friends;
    },

    componentDidMount: function()
    {
        if (!this.state.friends) {
            UserActions.fetchFriendsFor(this.props.user);
        }
    },

    render: function()
    {
        var friends = this.makeFriendsList();

        return (
            <div id="page_content">
                {friends}
            </div>
        );
    },

    makeFriendsList: function()
    {
        var friendList = this.state.friends;

        var friends = [];

        if (friendList !== undefined) {
            if (friendList.get('all').size > 0) {
                friendList.get('all').forEach(function (friend, i) {
                    friends.push(
                        <div key={i} className="box">
                            <UserInfoCard user={friend} />
                        </div>
                    );
                });
            } else {
                friends = <h1 className="box">{this.props.user.get('f_name')} dont have any friends yet :(</h1>;
            }
        }

        return friends;
    }
});