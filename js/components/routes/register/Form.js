// Modules
var React = require('react');

// Form templates
var RangeSelect = require('../../common/form/RangeSelect');
var MonthSelect = require('../../common/form/MonthSelect');
var FormErrors = require('../../common/form/FormErrors');
var RadioButtons = require('../../common/form/RadioButtons');

// Actions
var AuthActions = require('../../../actions/api/AuthActions');

// Stores
var AuthStore = global.AuthStore;
var ConfigStore = global.ConfigStore;

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

module.exports = React.createClass({
    mixins: [StoreWatcher],

    statics: {
        stores: [AuthStore],
        getState: function()
        {
            return {
                formErrors: AuthStore.getRegisterErrors(),
                formValues: AuthStore.getRegisterValues()
            };
        }
    },

    render: function()
    {
        return (
            <div id="registerForm" className="box">
                <FormErrors errors={this.state.formErrors} />
                <form onSubmit={this._onSubmit}>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <input
                                        onChange={this.updateForm}
                                        type="text"
                                        name="f_name"
                                        placeholder="First name"
                                        value={this.state.formValues.f_name}
                                    />
                                </td>
                                <td>
                                    <input
                                        onChange={this.updateForm}
                                        type="text"
                                        name="l_name"
                                        placeholder="Last name"
                                        value={this.state.formValues.l_name}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="2">
                                    <input
                                        onChange={this.updateForm}
                                        type="email"
                                        name="email"
                                        placeholder="email"
                                        value={this.state.formValues.email}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <RadioButtons
                                        onChange={this.updateForm}
                                        name="gender"
                                        values={["Male", "Female"]}
                                        checked={this.state.formValues.gender}
                                    />
                                </td>
                                <td>
                                    <RangeSelect
                                        _onChange={this.updateForm}
                                        name="birth_year"
                                        selected={this.state.formValues.birth_year}
                                        first={ConfigStore.youngestAllowedBirthDate}
                                        last={ConfigStore.oldestAllowedBirthDate}
                                    />
                                    <MonthSelect
                                        _onChange={this.updateForm}
                                        name="birth_month"
                                        selected={this.state.formValues.birth_month}
                                    />
                                    <RangeSelect
                                        _onChange={this.updateForm}
                                        name="birth_day"
                                        selected={this.state.formValues.birth_day}
                                        first={1}
                                        last={31}/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input
                                        onChange={this.updateForm}
                                        type="password"
                                        name="password"
                                        placeholder="Password"
                                        value={this.state.formValues.password}
                                    />
                                </td>
                                <td>
                                    <input
                                        onChange={this.updateForm}
                                        type="password"
                                        name="password_confirmation"
                                        placeholder="Re-enter password"
                                        value={this.state.formValues.password_confirmation}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>Register with facebook</td>
                                <td>
                                    <input
                                        type="submit"
                                        value="register"
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        );
    },

    updateForm: function(e)
    {
        var key = e.target.name;
        var value = e.target.value;

        this.setState({
            formValues: this.state.formValues.set(key, value)
        });
    },

    _onSubmit: function(e)
    {
        e.preventDefault();

        AuthActions.register(this.state.formValues);
    }
});