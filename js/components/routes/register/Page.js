var React = require('react');
var RegisterForm = require('./Form');

// Stores
var AuthStore = global.AuthStore;

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

module.exports = React.createClass({
    mixins: [StoreWatcher],

    statics: {
        stores: [AuthStore],
        getState: function()
        {
            return {
                unauthorized: AuthStore.isUnauthorized()
            };
        }
    },

    shouldComponentUpdate: function(nextProps, nextState)
    {
        return this.state.unauthorized !== nextState.unauthorized;
    },

    render: function()
    {
        if (AuthStore.isAuthorized()) {
            this.props.history.goBack();
        }

        return (
            <div id="page">
                <div className="box">
                    <h1>Register</h1>
                </div>
                <RegisterForm />
            </div>
        );
    }
});
