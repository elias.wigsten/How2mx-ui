var React = require('react');
var PropTypes = React.PropTypes;

// Templates
var InfoPopup = require('../../common/popups/Info');

module.exports = React.createClass({
    PropTypes: {
        onClose: PropTypes.func.isRequired
    },

    getInitialState: function()
    {
        return {
            title: 'Welcome to how2mx!',
            message: 'We have sent you an confirmation email, please confirm your identity by clicking the confirmation link to join the action.'
        };
    },

    render: function()
    {
        return (
            <InfoPopup
            onClose={this.props.onClose}
            title={this.state.title}
            message={this.state.message}
            />
        );
    }
});