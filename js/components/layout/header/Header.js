var React = require('react');
var Link = require('react-router').Link;
var MenuContext = require('../../../../assets/lang/mainMenu');

module.exports = React.createClass({
    getInitialState: function()
    {
        return {menuItems: MenuContext.en};
    },

    render: function()
    {
        return (
            <div id="header">
                <div className="centered">
                    <Link to="/"><img id="header_logo" src="http://localhost:8888/How2mx/project/storage/site/images/logo.png"/></Link>
                    <ul>
                        <li>{this.state.menuItems.shop}</li>
                        <li>{this.state.menuItems.troubleshoot}</li>
                        <li>{this.state.menuItems.howTo}</li>
                        <li>{this.state.menuItems.specifications}</li>
                        <li>{this.state.menuItems.tracks}</li>
                    </ul>
                </div>
            </div>
        );
    }
});