var React = require('react');

// Mixins
var StoreWatcher = require('../../mixins/StoreWatcher');

// Stores
var PopupStore = global.PopupStore;

// Constants
var PopupActions = require('../../actions/PopupActions');

module.exports = React.createClass({
    mixins: [StoreWatcher],

    statics: {
        stores: [PopupStore],
        getState: function()
        {
            return {
                popup: PopupStore.getPopup(),
                payload: PopupStore.getPayload()
            };
        }
    },

    render: function()
    {
        var Popup = this.state.popup;

        if (Popup === null) {
            return false;
        }

        return (
            <Popup payload={this.state.payload} onClose={this.onClose} />
        );
    },

    onClose: function()
    {
        PopupActions.closePopup();
    }
});