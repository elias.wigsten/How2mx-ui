var React = require('react');
var PropTypes = React.PropTypes;
var Link = require('react-router').Link;

// Actions
var AuthActions = require('../../../actions/api/AuthActions');

module.exports = React.createClass({
    PropTypes: {
        error: PropTypes.string.isRequired
    },

    getInitialState: function()
    {
        return {
            email: '',
            password: '',
            remember: false
        };
    },

    render: function()
    {
        var error = this.buildError();

        return (
            <div className="box login">
                <form onSubmit={this._onLogin}>
                    <table>
                        <tbody>
                            <tr>
                                <td colSpan="2">
                                    <input onChange={this._onChange} name="email" type="email" placeholder="E-mail"/>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="2">
                                    <input onChange={this._onChange} name="password" type="password" placeholder="Password" value={this.state.password}/>
                                </td>
                            </tr>
                            {error}
                            <tr>
                                <td>
                                    <input onChange={this._onRemember} type="checkbox" name="remember" id="remember"/>
                                    <label htmlFor="remember">Remember me</label>
                                </td>
                                <td>
                                    <input type="submit" name="submit" value="Login"/>
                                </td>
                            </tr>
                            <tr>
                                <td className="centerText">
                                    <Link to="/register">Register</Link>
                                </td>
                                <td>
                                    <p>Lost Password</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        );
    },

    buildError: function()
    {
        if (this.props.error !== undefined) {
            return (
                <tr>
                    <td  colSpan="2">{this.props.error}</td>
                </tr>
            );
        }
        else {
            return false;
        }
    },

    _onChange: function(e)
    {
        var key = e.target.name;
        var state = this.state;

        if (state.hasOwnProperty(key)) {
            state[key] = e.target.value;
        }

        this.setState(state);
    },

    _onRemember: function(e)
    {
        this.setState({
            remember: e.target.checked
        });
    },

    _onLogin: function(e)
    {
        e.preventDefault();

        var auth = this.state;
        this.setState({
            password: ''
        });

        AuthActions.login(auth);
    }
});