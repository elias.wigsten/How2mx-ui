var React = require('react');

// Components
var AuthBox = require('./AuthBox');
var List = require('./List');

module.exports = React.createClass({
    render: function()
    {
        return (
            <div id="sidebar">
                <AuthBox />
                <List />
            </div>
        );
    }
});
