var React = require('react');
var Link = require('react-router').Link;

// Actions
var AuthActions = require('../../../actions/api/AuthActions');

module.exports = React.createClass({
    getInitialState: function()
    {
        return {
            f_name:     this.props.user.f_name,
            l_name:     this.props.user.l_name,
            page_id:  this.props.user.page_id
        };
    },

    render: function()
    {
        return (
            <div className="box currentUser">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <Link className="profile_link" to={`user/${this.state.page_id}`}>
                                    <img className="smallProfileImg" src="http://localhost:8888/How2mx/project/storage/site/images/defaultProfileImage.png"/>
                                </Link>
                            </td>
                            <td colSpan="4">
                                <Link className="profile_link" to={`user/${this.state.page_id}`}>
                                    {this.props.user.f_name + " " + this.props.user.l_name}
                                </Link>
                            </td>
                        </tr>
                        <tr>
                            <td><img id="inboxImg" onClick={this.onMessages} src="http://localhost:8888/How2mx/project/storage/site/images/inbox.png"/></td>
                            <td><img id="inboxImg" onClick={this.onFriendRequests} src="http://localhost:8888/How2mx/project/storage/site/images/friendRequests.png"/></td>
                            <td><img id="inboxImg" onClick={this.onNotifications} src="http://localhost:8888/How2mx/project/storage/site/images/notifications.png"/></td>
                            <td><img id="inboxImg" onClick={this.onSettings} src="http://localhost:8888/How2mx/project/storage/site/images/settings.png"/></td>
                            <td><img id="inboxImg" onClick={this.onLogout} src="http://localhost:8888/How2mx/project/storage/site/images/logout.png"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    },

    onLogout: function()
    {
        AuthActions.logout();
    },

    onSettings: function()
    {
        console.log('settings!');
    },

    onNotifications: function()
    {
        console.log('notifications!');
    },

    onFriendRequests: function()
    {
        console.log('friend requests!');
    },

    onMessages: function()
    {
        console.log('messages!');
    }
});
