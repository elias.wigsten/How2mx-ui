var React = require('react');
var PropTypes = React.PropTypes;

// Components
var LoginComponent = require('./Login');
var UserComponent = require('./User');

// Mixins
var StoreWatcher = require('../../../mixins/StoreWatcher');

// Stores
var AuthStore = global.AuthStore;

module.exports = React.createClass({
    contextTypes: {
        user: PropTypes.object.isRequired
    },

    mixins: [StoreWatcher],
    statics: {
        stores: [AuthStore],
        getState: function()
        {
            return {
                loginError: AuthStore.getLoginError()
            };
        }
    },

    render: function()
    {
        if (this.context.user.id) {
            return (
                <UserComponent user={this.context.user} />
            );
        }

        return (
            <LoginComponent error={this.state.loginError.error} />
        );
    }
});