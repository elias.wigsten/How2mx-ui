var React = require('react');
var render = require('react-dom').render;
var Router = require('react-router').Router;

//const createBrowserHistory = require('history/lib/createBrowserHistory'); For removing /#/xxxxxxx/

// Stores
global.AuthStore = require('./stores/AuthStore');
global.SelfStore = require('./stores/SelfStore');
global.ConfigStore = require('./stores/ConfigStore');
global.PopupStore = require('./stores/PopupStore');
global.UserStore = require('./stores/UserStore');
global.ApiStore = require('./stores/static/Api');

var Routes = require('./routes');

render((
        <Router>
            {Routes}
        </Router>
    ),
    document.getElementById('body_wrapper')
);