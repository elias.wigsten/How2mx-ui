// Dispatchers
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ApiDispatcher = require('../dispatcher/ApiDispatcher');

// Event emitter
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var Immutable = require('immutable');
var CHANGE_EVENT = 'change';

// Constants
var AuthConstants = require('../constants/AuthConstants');
var UserConstants = require('../constants/UserConstants');
var GalleryConstants = require('../constants/GalleryConstants');

// Actions
var Api = require('../actions/Api');

// Utils
var DateUtils = require('../utils/DateUtils');

// Models
var UserModel = require('../models/User');
var UserMapModel = require('../models/UserMap');
var GalleryListModel = require('../models/GalleryList');
var GalleryModel = require('../models/Gallery');
var FriendListModel = require('../models/FriendList');
var GalleryItemList = require('../models/GalleryItemList');
var GalleryItem = require('../models/GalleryItem');

var SelfStore = assign({}, EventEmitter.prototype, {
    Self: undefined,
    NewGallery: undefined,

    setSelf: function(userData)
    {
        var user = userData ? UserModel.createUser(userData) : UserModel.createGuest();
        this.Self = new UserMapModel({
            info: user,
            galleries: GalleryListModel.init()
        });

        this.emitChange();
    },

    setGalleries: function(response)
    {
        console.log(response);
        if (this.owns(response.page_id)) {
            var galleries = this.Self.galleries.addMany(response.galleries);
            galleries = galleries.set('canRefresh', response.canRefresh);

            this.Self = this.Self.set('galleries',
                galleries
            );

            this.emitChange();
        }
    },

    addGallery: function(gallery)
    {
        if (this.owns(gallery.page_id)) {
            var galleries = this.Self.galleries.add(gallery);
            this.setUserField('galleries', galleries);
        }
    },

    addGalleryItems: function(gallery)
    {
        if (this.owns(gallery.page_id)) {
            this.Self = this.Self.set('galleries',
                this.Self.galleries.replace(gallery)
            );

            this.emitChange();
        }
    },

    setNewGallery: function(gallery)
    {
        if (gallery) {
            this.NewGallery = gallery;
        }
    },

    createUserGallery: function(gallery)
    {
        if (gallery && gallery.page_id) {
            if (this.owns(gallery.page_id)) {
                gallery = GalleryModel.create(gallery);
                gallery = gallery.set('files_to_upload', this.UToUpload());

                var galleries = GalleryListModel.add(gallery, this.Self.galleries);
                this.Self = this.Self.set('galleries', galleries);

                this.setNewGallery(gallery);

                this.emitChange();
            }
        }
    },

    addItemToUserGallery: function(item)
    {
        item = GalleryItem.create(item);
        var gallery = this.Self.galleries.all.get(item.gallery_id);

        var updatedGallery = GalleryModel.addOneItem(item, gallery);
        var updatedGalleries = GalleryListModel.replaceOne(updatedGallery, this.Self.galleries);

        this.Self = this.Self.set('galleries', updatedGalleries);

        this.emitChange();
    },

    addFilesToUpload: function(files)
    {
        if (this.Self && this.Self.info) {
            var gallery = GalleryModel.create({page_id: this.Self.info.page_id});
            this.NewGallery = gallery.set('files_to_upload', files);
        }
    },

    setFriends: function(friends)
    {
        friends = FriendListModel.create(friends);
        this.Self = this.Self.set('friends', friends);

        this.emitChange();
    },

    addFriend: function(friend)
    {
        var updatedUser = FriendListModel.add(friend, this.Self);

        this.update(updatedUser);
    },

    /* ---------------------------------------- UPDATES ---------------------------------------- */

    updateUserGallery: function(gallery)
    {
        if (this.owns(gallery.page_id)) {
            var currentGallery = this.Self.galleries.all.get(gallery.id);

            if (currentGallery === gallery) {}
            this.Self = this.Self.set('galleries',
                this.Self.galleries.replace(gallery)
            );
        }
    },

    updateGalleryItemDescription: function(item)
    {
        if (item) {
            var gallery = this.Self.galleries.all.get(item.gallery_id);

            if (gallery) {
                gallery = gallery.updateItemDescription(item);

                this.Self = this.Self.set('galleries',
                    this.Self.galleries.replace(gallery)
                );

                this.emitChange();
            }
        }
    },

    /* ---------------------------------------- GETTERS ---------------------------------------- */

    getPageId: function() {
        return this.getUser().page_id;
    },

    getUser: function()
    {
        return this.Self ? this.Self.get('info') : undefined;
    },

    getGalleries: function()
    {
        return this.Self.get('galleries');
    },

    getGallery: function(gallery_token)
    {
        return this.Self.galleries.all.get(gallery_token);
    },

    getNewGallery: function()
    {
        var gallery = this.NewGallery;

        if (gallery && gallery.id) {
            this.NewGallery = undefined;
        }

        return gallery;
    },

    getFilesToUpload: function()
    {
        if (this.NewGallery && this.NewGallery.files_to_upload) {
            var files = this.NewGallery.files_to_upload;
            this.NewGallery.remove('files_to_upload');

            return files;
        }
    },

    getFriends: function()
    {
        return this.Self.get('friends');
    },

    /* ---------------------------------------- DELETES ---------------------------------------- */

    removeFriend: function(friend)
    {
        var updatedUser = FriendListModel.remove(friend, this.Self);

        this.update(updatedUser);
    },

    deleteGalleryItems: function(items)
    {
        if (items.length > 0) {
            var gallery = this.Self.galleries.all.get(items[0].gallery_id);
            gallery = gallery.removeItems(items);

            this.Self = this.Self.set('galleries',
                this.Self.galleries.replace(gallery)
            );

            this.emitChange();
        }
    },

    resetStore: function()
    {
        this.NewGallery = undefined;
        this.Self = this.setSelf();
    },

    /* ---------------------------------------- HELPERS ---------------------------------------- */

    update: function(updatedUser)
    {
        if (updatedUser && updatedUser !== this.Self) {
            this.Self = updatedUser;
            this.emitChange();
        }
    },

    owns: function(page_id)
    {
        return this.Self && this.Self.info && this.Self.info.page_id == page_id;
    },

    getAuthLevel: function()
    {
        return this.Self.info.get('auth_level');
    },

    isUser: function()
    {
        return this.Self && this.getAuthLevel() === "USER";
    },

    isGuest: function()
    {
        return this.Self != undefined;
    },

    setUserField: function(field, value)
    {
        this.Self = this.Self.set(field, value);
        this.emitChange();
    },

    /* ---------------------------------------- EVENT EMITTER ---------------------------------------- */

    addChangeListener: function(callback, component)
    {
        // console.log('SelfStore add listener: ', component);
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback, component)
    {
        // console.log('SelfStore remove listener: ', component);
        this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function()
    {
        this.emit(CHANGE_EVENT);
    }
});

/*
SelfStore.dispatchToken = ApiDispatcher.register( function(payload) {
    var action = payload.action;

    switch(action.type) {
        case GalleryConstants.CREATED_USER_GALLERY:
            SelfStore.createUserGallery(action.gallery);
            break;
        case GalleryConstants.UPLOADED_FILE:
            SelfStore.addItemToUserGallery(action.item);
            break;
        case GalleryConstants.UPDATED_ITEM_DESCRIPTION:
            SelfStore.updateGalleryItemDescription(action.item);
            break;
        case GalleryConstants.FETCHED_USER_GALLERIES:
            SelfStore.setGalleries(action.response);
            break;
        case GalleryConstants.FETCHED_USER_GALLERY:
            SelfStore.addGallery(action.gallery);
            break;
        case GalleryConstants.FETCHED_USER_GALLERY_ITEMS:
            SelfStore.addGalleryItems(action.gallery);
            break;
        case GalleryConstants.SAVED_USER_GALLERY_CHANGES:
            SelfStore.updateUserGallery(action.gallery);
            break;
        default:
            break;
    }
});*/

SelfStore.dispatchToken = AppDispatcher.register( function(payload) {
    var action = payload.action;

    switch(action.type) {
        case AuthConstants.LOGIN_SUCCESS:
            SelfStore.setSelf(action.user);
            break;
        case AuthConstants.UNAUTHORIZED:
            SelfStore.setSelf();
            break;
        case AuthConstants.LOGOUT:
            SelfStore.resetStore();
            break;
        /*case UserConstants.NEW_GALLERY:
            SelfStore.addFilesToUpload(action.files);
            break;*/
        case UserConstants.FETCHED_FRIENDS:
            SelfStore.setFriends(action.data.friends);
            break;
        case UserConstants.FRIEND_REQUEST_SENT:
            SelfStore.addFriend(action.friend);
            break;
        case UserConstants.FRIEND_REQUEST_ACCEPTED:
            SelfStore.removeFriend(action.friend);
            break;
        case GalleryConstants.DELETE_ITEMS:
            SelfStore.deleteGalleryItems(action.items);

            Api.user.galleries.items.deleteMany(action.items);
            break;
        default:
    }

    return true;
});

module.exports = SelfStore;