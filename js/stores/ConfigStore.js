var ages = {
    oldest: 70,
    youngest: 13
};

var currentYear = new Date().getFullYear();

module.exports = {
    youngestAllowedBirthDate: currentYear - ages.youngest,
    oldestAllowedBirthDate: currentYear - ages.oldest,
    apiRootUrl: 'http://localhost:8888/How2mx/project/API/public/',
    storage: {
        url: {
            root: 'http://localhost:8888/How2mx/project/storage/',
            pages: function()
            {
                return this.root + 'pages/';
            }
        }
    }
};