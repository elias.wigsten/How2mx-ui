"use strict";
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ApiDispatcher = require('../dispatcher/ApiDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var Immutable = require('immutable');
var CHANGE_EVENT = 'change';

// Constants
var AuthConstants = require('../constants/AuthConstants');
var UserConstants = require('../constants/UserConstants');

// Models
var PostList = require('../models/PostList');
var Post = require('../models/Post');

var PostStore = assign({}, EventEmitter.prototype, {
    PagePosts: Immutable.OrderedMap(),

    /* ---------------------------------------- GETTERS ---------------------------------------- */

    getPostsFor: function(page_id)
    {
        return this.PagePosts.get(page_id);
    },

    /* ---------------------------------------- SETTERS ---------------------------------------- */

    setPosts: function(posts)
    {
        let pagePosts = this.getPostsFor(posts.page_id);
        pagePosts = !pagePosts ? posts : pagePosts.syncWith(posts);

        this.insert(
            pagePosts.page_id,
            pagePosts
        );

        this.emitChange();
    },

    addPost: function(post)
    {
        this.insert(
            post.recipient.id,
            this.PagePosts.get(post.recipient.id).addOne(post)
        );

        this.emitChange();
    },

    /* ---------------------------------------- DELETES ---------------------------------------- */

    deletePost: function(post)
    {
        this.deletePostFor(post.author.id, post.id);

        if (post.author.id !== post.recipient.id) {
            this.deletePostFor(post.recipient.id, post.id);
        }

        this.emitChange();
    },

    deletePostFor: function(page_id, post_id)
    {
        this.insert(
            page_id,
            this.PagePosts.get(page_id).deleteOne(post_id)
        );
    },

    /* ---------------------------------------- HELPERS ---------------------------------------- */

    insert: function(page_id, posts)
    {
        this.PagePosts = this.PagePosts.set(page_id, posts);
    },

    /* ---------------------------------------- EVENT EMITTER ---------------------------------------- */

    addChangeListener: function(callback, component)
    {
        //console.log('add listener: ', component);
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback, component)
    {
        //console.log('remove listener: ', component);
        this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function()
    {
        //console.log('STORE CHANGED : - : - : ', this.event);
        this.emit(CHANGE_EVENT);
    }
});

PostStore.dispatchToken = AppDispatcher.register( function(payload) {
    var action = payload.action;

    switch(action.type) {
        case UserConstants.FETCHED_USER_POSTS:
            PostStore.setPosts(action.posts);
            break;
        case UserConstants.FETCHED_POSTS:
            PostStore.setPosts(action.data);
            break;
        case UserConstants.CREATED_POST:
            PostStore.addPost(action.post);
            break;
        case UserConstants.DELETED_POST:
            //AppDispatcher.waitFor([SelfStore.dispatchToken]);

            PostStore.deletePost(action.post);
            break;
        /*case UserConstants.FETCHED_POSTS:
            UserPageStore.updatingSelfStore();
            break;*/
        default:
            break;
    }

    return true;
});

module.exports = PostStore;