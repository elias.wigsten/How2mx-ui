var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var Immutable = require('immutable');

// Constants
var AuthConstants = require('../constants/AuthConstants');
var PopupConstants = require('../constants/PopupConstants');

// Popups
var RegisterSuccessPopup = require('../components/routes/register/RegisterSuccessPopup');

// Static stores
var Config = require('./ConfigStore');

var CHANGE_EVENT = 'change';

var PopupStore = assign({}, EventEmitter.prototype, {
    CurrentPopup: null,
    Payload: null,

    openRegistered: function()
    {
        this.setPopup(RegisterSuccessPopup);
    },

    /* ---------------------------------------- GETTERS ---------------------------------------- */

    getPopup: function()
    {
        return this.CurrentPopup;
    },

    getPayload: function()
    {
        return this.Payload;
    },

    /* ---------------------------------------- HELPERS ---------------------------------------- */

    setPopup: function(popup, payload)
    {
        if (popup && this.CurrentPopup === null) {
            this.CurrentPopup = popup;
            this.Payload = payload ? payload : null;

            this.emitChange();
        }
    },

    close: function()
    {
        this.CurrentPopup = null;
        this.Payload = null;
        this.emitChange();
    },

    /* ---------------------------------------- EVENT EMITTER ---------------------------------------- */

    addChangeListener: function(callback)
    {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback)
    {
        this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function()
    {
        this.emit(CHANGE_EVENT);
    }
});

PopupStore.dispatchToken = AppDispatcher.register( function(payload) {
    var action = payload.action;

    switch (action.type) {
        case AuthConstants.REGISTER_SUCCESS:
            PopupStore.openRegistered();
            break;
        case PopupConstants.CLOSE_POPUP:
            PopupStore.close();
            break;
        default:
    }
});

module.exports = PopupStore;