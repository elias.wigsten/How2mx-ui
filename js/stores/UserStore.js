var AppDispatcher = require('../dispatcher/AppDispatcher');
var ApiDispatcher = require('../dispatcher/ApiDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var Immutable = require('immutable');
var CHANGE_EVENT = 'change';

// Constants
var AuthConstants = require('../constants/AuthConstants');
var UserConstants = require('../constants/UserConstants');
var GalleryConstants = require('../constants/GalleryConstants');

// Models
var UserMapModel = require('../models/UserMap');
var UserModel = require('../models/User');
var GalleryListModel = require('../models/GalleryList');
var FriendListModel = require('../models/FriendList');
var FriendshipModel = require('../models/Friendship');

// Stores
var SelfStore = global.SelfStore;

var UserPageStore = assign({}, EventEmitter.prototype, {
    Users: new Immutable.OrderedMap(),

    /* ---------------------------------------- SETTERS ---------------------------------------- */

    setUser: function(user)
    {
        if (this.exists(user)) {
            this.updateUser(user);
        } else {
            this.addUser(user);
        }
    },

    addUser: function(user)
    {
        var userMap = new UserMapModel({
            info: UserModel.createUser(user),
            galleries: GalleryListModel.init()
        });

        this.Users = this.Users.set(userMap.info.get('page_id'), userMap);

        this.emitChange();
    },

    updateUser: function(user)
    {
        if (this.userHasChanged(user)) {
            this.addUser(user);
        }
    },

    setFriends: function(data)
    {
        var friends = FriendListModel.create(data.friends);

        this.setUserField(data.page_id, 'friends', friends);
    },

    addFriend: function(friend)
    {
        var self = SelfStore.getUser();

        var page_id = friend.get('page_id');
        var user = this.Users.get(page_id);

        var updatedUser = FriendshipModel.setPending(user, self);

        this.update(page_id, updatedUser);
    },

    acceptFriend: function(friend)
    {
        var self = SelfStore.getUser();

        var page_id = friend.get('page_id');
        var user = this.Users.get(page_id);

        var tempUser = FriendListModel.add(self, user);
        tempUser = tempUser ? tempUser : user;

        var updatedUser = FriendshipModel.setAccepted(tempUser, self);

        this.update(page_id, updatedUser);
    },

    setGalleries: function(response)
    {
        if (!SelfStore.owns(response.page_id)) {
            var user = this.Users.get(response.page_id);

            if (user) {
                var galleries = user.galleries.addMany(response.galleries);
                galleries = galleries.set('canRefresh', response.canRefresh);

                this.setUserField(
                    user.info.page_id,
                    'galleries',
                    galleries
                );
            }
        }
    },

    addGallery: function(gallery)
    {
        var user = this.Users.get(gallery.page_id);

        if (user) {
            var galleries = user.galleries.add(gallery);

            this.setUserField(
                user.info.page_id,
                'galleries',
                galleries
            );
        } else {
            this.emitChange();
        }
    },

    addGalleryItems: function(gallery)
    {
        var user = this.Users.get(gallery.page_id);

        if (user) {
            this.setUserField(
                gallery.page_id,
                'galleries',
                user.galleries.replace(gallery)
            );
        }
    },

    /* ---------------------------------------- GETTERS ---------------------------------------- */

    getUser: function(page_id)
    {
        if (SelfStore.owns(page_id)) {
            return SelfStore.getUser();
        }

        return this.Users.get(page_id) ? this.Users.get(page_id).get('info') : undefined;
    },

    getPreviewCard: function(page_id)
    {

    },

    getGalleriesFor: function(page_id)
    {
        if (SelfStore.owns(page_id)) {
            return SelfStore.getGalleries();
        }

        return this.Users.get(page_id) ? this.Users.get(page_id).get('galleries') : undefined;
    },

    getGallery: function(page_id, gallery_id)
    {
        if (SelfStore.owns(page_id)) {
            return SelfStore.getGallery(gallery_id);
        }

        var user = this.Users.get(page_id);

        if (user) {
            return user.galleries.all.get(gallery_id);
        }
    },

    getFriendsFor: function(page_id)
    {
        if (SelfStore.owns(page_id)) {
            return SelfStore.getFriends();
        }

        return this.Users.get(page_id) ? this.Users.get(page_id).get('friends') : undefined;
    },

    /* ---------------------------------------- DELETES ---------------------------------------- */

    removeFriend: function(friend)
    {
        var self = SelfStore.getUser();

        var page_id = friend.get('page_id');
        var user = this.Users.get(page_id);

        var tempUser = FriendListModel.remove(self, user);
        tempUser = tempUser ? tempUser : user;

        var updatedUser = FriendshipModel.clearFriendshipFor(tempUser);

        this.update(page_id, updatedUser);
    },

    removeSelf: function(self)
    {
        this.Users = this.Users.remove(self.page_id);
        this.emitChange();
    },

    resetStore: function()
    {
        this.Users = new Immutable.OrderedMap();
        this.emitChange();
    },

    /* ---------------------------------------- HELPERS ---------------------------------------- */

    setUserField: function(page_id, field, value)
    {
        var user = this.Users.get(page_id);

        if (user && user.get(field) !== value) {
            var updatedUser = user.set(field, value);

            this.Users = this.Users.set(page_id, updatedUser);
        }

        this.emitChange();
    },

    update: function(page_id, updatedUser)
    {
        if (updatedUser && updatedUser !== this.Users.get(page_id)) {
            this.Users = this.Users.set(page_id, updatedUser);
            this.emitChange();
        }
    },

    exists: function(user)
    {
        return this.Users.get(user.page_id) !== undefined;
    },

    userHasChanged: function(user)
    {
        var currentUser = this.Users.get(user.page_id).get('info');

        if (currentUser.f_name !== user.f_name) return true;
        if (currentUser.l_name !== user.l_name) return true;
        if (currentUser.age !== user.birth_date) return true;
        if (currentUser.friendship === undefined && user.friendship !== undefined) return true;
        if (currentUser.friendship) {
            if (currentUser.friendship.status !== user.friendship.status) return true;
            if (currentUser.friendship.user1 !== user.friendship.user1) return true;
            if (currentUser.friendship.user2 !== user.friendship.user2) return true;
        }

        return currentUser.profile_img !== user.profile_img;
    },

    shouldUpdate: function(page_id)
    {
        if (!SelfStore.owns(page_id)) {
            return this.Users.get(page_id) === undefined;
        }

        return false;
    },

    /* ---------------------------------------- EVENT EMITTER ---------------------------------------- */

    updatingSelfStore: function()
    {
        AppDispatcher.waitFor([SelfStore.dispatchToken]);

        this.emitChange();
    },

    addChangeListener: function(callback, component)
    {
        // console.log('UserStore add listener: ', component);
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback, component)
    {
        // console.log('UserStore remove listener: ', component);
        this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function()
    {
        this.emit(CHANGE_EVENT);
    }
});

/*UserPageStore.dispatchToken = ApiDispatcher.register( function(payload) {
    var action = payload.action;

    switch(action.type) {
        case GalleryConstants.FETCHED_USER_GALLERIES:
            UserPageStore.setGalleries(action.response);
            break;
        case GalleryConstants.FETCHED_USER_GALLERY:
            UserPageStore.addGallery(action.gallery);
            break;
        case GalleryConstants.FETCHED_USER_GALLERY_ITEMS:
            UserPageStore.addGalleryItems(action.gallery);
            break;
        default:
            break;
    }
});*/

UserPageStore.dispatchToken = AppDispatcher.register( function(payload) {
    var action = payload.action;

    switch (action.type) {
        case AuthConstants.LOGIN_SUCCESS:
            UserPageStore.removeSelf(action.user);
            break;
        case AuthConstants.LOGOUT:
            UserPageStore.resetStore();
            break;
        case UserConstants.FETCHED_USER_PROFILE:
            UserPageStore.setUser(action.user);
            break;
        case UserConstants.FETCHED_USER_FRIENDS:
            UserPageStore.setFriends(action.data);
            break;
        case UserConstants.FRIEND_REQUEST_SENT:
            UserPageStore.addFriend(action.friend);
            break;
        case UserConstants.FRIEND_REQUEST_ACCEPTED:
            UserPageStore.acceptFriend(action.friend);
            break;
        case UserConstants.FRIEND_REMOVED:
            UserPageStore.removeFriend(action.friend);
            break;
        case UserConstants.FETCHED_FRIENDS:
            UserPageStore.updatingSelfStore();
            break;
        case UserConstants.CREATED_STATUS:
            UserPageStore.updatingSelfStore();
            break;
        default:
    }

    return true;
});

module.exports = UserPageStore;