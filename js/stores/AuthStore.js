var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var Immutable = require('immutable');

// Constants
var AuthConstants = require('../constants/AuthConstants');

// Static stores
var Config = require('./ConfigStore');

var CHANGE_EVENT = 'change';

var RegisterValues = Immutable.Record({
    f_name: undefined,
    l_name: undefined,
    email: undefined,
    gender: undefined,
    birth_year: Config.youngestAllowedBirthDate.toString(),
    birth_month: "1",
    birth_day: "1",
    password: undefined,
    password_confirmation: undefined
});

var RegisterErrorInput = Immutable.Record({
    key: undefined,
    errors: Immutable.List()
});

var Error = Immutable.Record({
    error: undefined
});

var RegisterError = Immutable.Record({
    error: undefined
});

var ShowPopups = Immutable.Record({
    register: false
});

var AuthStore = assign({}, EventEmitter.prototype, {
    RegisterValues:     new RegisterValues(),
    RegisterErrors:     null,
    LoginError:         new Error(),
    authorized:         undefined,

    isAuthorized: function()
    {
        return this.authorized;
    },

    isUnauthorized: function()
    {
        if (this.authorized !== undefined) {
            return !this.authorized
        }
    },

    setAuthorized: function(authorized)
    {
        this.authorized = authorized;

        this.emitChange();
    },

    getLoginError: function()
    {
        return this.LoginError;
    },

    loginFailed: function(error)
    {
        this.LoginError = this.LoginError.set('error', error);
        this.emitChange();
    },

    resetLoginState: function()
    {
        this.LoginError = this.LoginError.remove('error');
    },

    /* ---------------------------------------- START OF REGISTER ---------------------------------------- */

    addRegisterErrors: function(errors, formValues)
    {
        this.RegisterErrors = errors;
        this.RegisterValues = formValues;
        this.emitChange();
    },

    getRegisterErrors: function()
    {
        return this.RegisterErrors;
    },

    getRegisterValues: function()
    {
        return this.RegisterValues;
    },

    registeredUserSuccessfully: function()
    {
        this.RegisterErrors = null;
        this.RegisterValues = new RegisterValues();
        this.resetRegisterState();

        this.emitChange();
    },

    resetRegisterState: function()
    {
        this.RegisterValues = new RegisterValues();
        this.RegisterErrors = null;
        this.emitChange();
    },

    /* ---------------------------------------- EVENT EMITTER ---------------------------------------- */

    addChangeListener: function(callback)
    {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback)
    {
        this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function()
    {
        this.emit(CHANGE_EVENT);
    }
});

AuthStore.dispatchToken = AppDispatcher.register( function(payload) {
    var action = payload.action;

    switch (action.type) {
        case AuthConstants.LOGIN_SUCCESS:
            AuthStore.resetLoginState();
            AuthStore.setAuthorized(true);
            break;
        case AuthConstants.LOGOUT:
            AuthStore.resetLoginState();
            AuthStore.setAuthorized(false);
            break;
        case AuthConstants.UNAUTHORIZED:
            AuthStore.setAuthorized(false);
            break;
        case AuthConstants.LOGIN_FAILED:
            AuthStore.loginFailed(action.error);
            break;
        case AuthConstants.REGISTER_FAILED:
            AuthStore.addRegisterErrors(action.formErrors, action.formValues);
            break;
        case AuthConstants.REGISTER_SUCCESS:
            AuthStore.registeredUserSuccessfully();
            break;
        case AuthConstants.RESET_REGISTER_STATE:
            AuthStore.resetRegisterState();
            break;
        default:
    }

    return true;
});

module.exports = AuthStore;