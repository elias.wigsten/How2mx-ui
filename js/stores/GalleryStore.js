var AppDispatcher = require('../dispatcher/AppDispatcher');
var ApiDispatcher = require('../dispatcher/ApiDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var Immutable = require('immutable');
var CHANGE_EVENT = 'change';

// Constants
var AuthConstants = require('../constants/AuthConstants');
var UserConstants = require('../constants/UserConstants');
var GalleryConstants = require('../constants/GalleryConstants');

// Models
var GalleryListModel = require('../models/GalleryList');
var GalleryModel = require('../models/Gallery');

var AuthStore = require('../stores/SelfStore');

var GalleryStore = assign({}, EventEmitter.prototype, {
    Pages: new Immutable.OrderedMap(),
    NewGallery: undefined,

    setNewGallery: function(gallery)
    {
        if (gallery) {
            this.NewGallery = gallery;
            this.emitChange();
        }
    },

    addItem: function(item)
    {
        var gallery = this.getGallery(item.page_id, item.gallery_id);

        this.insertGallery(
            gallery.addItems([item])
        );
    },

    updateItem: function(item)
    {
        console.log('update item: ', item);
    },

    addFilesToUpload: function(files)
    {
        var user = AuthStore.getUser();
        if (user) {
            var gallery = GalleryModel.create({page_id: user.id});

            this.setNewGallery(
                gallery.set('files_to_upload', files)
            );
        }
    },

    addCreatedGallery: function(gallery)
    {
        var filesToUpload = this.getFilesToUpload();

        var galleries = this.findOrCreate(gallery.page_id);
        var newGalleries = galleries.createList([gallery]);

        galleries = galleries.unshift(newGalleries);
        this.NewGallery = galleries.all.get(gallery.id);

        this.insert(
            galleries.addFilesToUpload(gallery.id, filesToUpload)
        );
    },

    addGalleries: function(response)
    {
        var galleries = this.findOrCreate(response.page_id);
        var newGalleries = galleries.createList(response.galleries);

        if (galleries.all.size > 0) {
            if (newGalleries.first().created_at < galleries.all.last().created_at) {
                galleries = galleries.append(newGalleries);
            } else {
                galleries = galleries.unshift(newGalleries);
            }
        } else {
            galleries = galleries.setAll(newGalleries);
        }

        this.insert(
            galleries.set('canRefresh', response.canRefresh)
        );
    },

    addGallery: function(gallery)
    {
        var galleries = this.findOrCreate(gallery.page_id);
        galleries = galleries.add(gallery);

        this.insert(
            galleries
        );
    },

    addGalleryItems: function(gallery)
    {
        var galleries = this.findOrCreate(gallery.page_id);

        this.insert(
            galleries.replace(gallery)
        );
    },

    getNewGallery: function(page_id)
    {
        if (this.NewGallery && this.NewGallery.page_id === page_id) {
            var gallery = this.NewGallery;
            this.NewGallery = undefined;

            return gallery;
        }
    },

    getFilesToUpload: function()
    {
        if (this.NewGallery && this.NewGallery.files_to_upload) {
            var files = this.NewGallery.files_to_upload;
            this.NewGallery.remove('files_to_upload');

            return files;
        }
    },

    getGalleries: function(page_id)
    {
        return this.Pages.get(page_id);
    },

    getGallery: function(page_id, gallery_id)
    {
        var galleries = this.getGalleries(page_id);

        if (galleries) {
            return galleries.all.get(gallery_id);
        }
    },

    /* ---------------------------- HELPERS ---------------------------- */

    findOrCreate: function(page_id)
    {
        var galleries = this.Pages.get(page_id);

        return galleries ? galleries : GalleryListModel.init(page_id);
    },

    insert: function(galleries)
    {
        this.Pages = this.Pages.set(galleries.page_id, galleries);

        this.emitChange();
    },

    insertGallery: function(gallery)
    {
        var galleries = this.getGalleries(gallery.page_id);

        this.insert(
            galleries.replace(gallery)
        );
    },

    /* ---------------------------------------- EVENT EMITTER ---------------------------------------- */

    addChangeListener: function(callback, component)
    {
        //console.log('add listener: ', component);
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback, component)
    {
        //console.log('remove listener: ', component);
        this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function()
    {
        //console.log('STORE CHANGED : - : - : ', this.event);
        this.emit(CHANGE_EVENT);
    }
});

GalleryStore.dispatchToken = ApiDispatcher.register( function(payload) {
    var action = payload.action;

    switch (action.type) {
        case GalleryConstants.FETCHED_USER_GALLERIES:
            GalleryStore.addGalleries(action.response);
            break;
        case GalleryConstants.FETCHED_USER_GALLERY:
            GalleryStore.addGallery(action.gallery);
            break;
        case GalleryConstants.FETCHED_USER_GALLERY_ITEMS:
            GalleryStore.addGalleryItems(action.gallery);
            break;
        case GalleryConstants.UPLOADED_FILE:
            GalleryStore.addItem(action.item);
            break;
        default:
            break;
    }

    return true;
});

GalleryConstants.dispatchToken = AppDispatcher.register( function(payload) {
    var action = payload.action;

    switch (action.type) {
        case UserConstants.NEW_GALLERY:
            GalleryStore.addFilesToUpload(action.files);
            break;
        case GalleryConstants.CREATED_USER_GALLERY:
            GalleryStore.addCreatedGallery(action.gallery);
            break;
        case GalleryConstants.UPDATED_ITEM_DESCRIPTION:
            GalleryStore.updateItem(action.item);
            break;
        default:
            break;
    }

    return true;
});

module.exports = GalleryStore;