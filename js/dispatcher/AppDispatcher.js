var Dispatcher = require('flux').Dispatcher;
var assign = require('object-assign');

module.exports = assign(new Dispatcher(), {
    viewAction: function(action)
    {
        this.dispatchMiddleware({
            source: 'VIEW_ACTION',
            action: action
        });
    },

    serverAction: function(action)
    {
        this.dispatchMiddleware({
            source: 'SERVER_ACTION',
            action: action
        });
    },

    appAction: function(action)
    {
        this.dispatchMiddleware({
            source: 'APP_ACTION',
            action: action
        });
    },

    dispatchMiddleware: function(dispatch)
    {
        console.log(dispatch.action.type);
        this.dispatch(dispatch);
    }
});
